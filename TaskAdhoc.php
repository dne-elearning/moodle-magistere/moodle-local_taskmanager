<?php

defined('MOODLE_INTERNAL') || die;

class TaskAdhoc {
    private $task = null;

    function __construct($record)
    {
        $this->task = $record;
    }

    function endTask($status, $logs, $nextruntime, $startdate, $enddate, $exectime, $nbqueries) {
        global $DB;
        $adhoc_logs = new stdClass();
        $adhoc_logs->instance = $this->task->instance;
        $adhoc_logs->taskid = $this->task->taskid;
        $adhoc_logs->classname = $this->task->classname;
        $adhoc_logs->customdata = $this->task->customdata;
        $adhoc_logs->vmid = $this->task->runvmid;
        $adhoc_logs->status = $status;
        $adhoc_logs->workerid = $this->task->workerid;
        $adhoc_logs->nextruntime =$nextruntime;
        $adhoc_logs->startdate = $startdate;
        $adhoc_logs->enddate = $enddate;
        $adhoc_logs->nbqueries = $nbqueries;
        $adhoc_logs->exectime = $exectime;
        
        $recordid = $DB->insert_record(TaskManager::TABLE_ADHOC_LOG, $adhoc_logs);
        
        $rec_logs = new stdClass();
        $rec_logs->id = $recordid;
        $rec_logs->logs = $logs;
        
        $DB->insert_record_raw(TaskManager::TABLE_ADHOC_LOGS, $rec_logs, true, false, true);

        if ($status === TaskManager::STATUS_FAILED) {
            $this->task->attempts++;
            $this->task->workerid = null;
            if ($this->task->attempts >= TaskManager::ADHOC_MAX_ATTEMPTS) {
                $this->task->status = TaskManager::STATUS_FAILED;
            } else {
                $this->task->status = TaskManager::STATUS_WAITING;
                $this->task->nextruntime = time() + (rand(30,60)*$this->task->attempts);
            }
            $DB->update_record(TaskManager::TABLE_ADHOC, $this->task);
        } else {
            $DB->delete_records(TaskManager::TABLE_ADHOC, array('id' => $this->task->id));
        }
    }

    function execute(){
        $php = $this->task->phpbin;
        $aca = get_config('local_taskmanager', 'mag_academy_path');
        $command = $php.' '.$aca.'/'.$this->task->instance.'/local/taskmanager_agent/run_adhoc.php --execute="'.$this->task->taskid.'"';
        
        $starttime = microtime(true);
        $output = $err = '';
        $retval = $this->my_shell_exec($command, $output, $err);
        $stoptime = microtime(true);
        
        echo "OUTPUT CODE: ".$retval."\n";

        // Output look like a valid JSON
        if (!empty($output) && strpos($output,'{"error":') === 0){
            $json = json_decode($output);
            if ($json === null || $json->error == true){
                $this->endTask(TaskManager::STATUS_FAILED, 
                    "2###RETURN CODE: ".$retval."###\n###OUTPUT###\n".$output."\n######\n###ERROR###\n".$err."\n######",
                    $this->task->nextruntime,
                    round($starttime),
                    round($stoptime),
                    $stoptime-$starttime,
                    0
                );
            }else{
                $this->endTask(TaskManager::STATUS_SUCCESS, 
                    "1###RETURN CODE: ".$retval."###\n###OUTPUT###\n".base64_decode($json->data->logs)."\n######\n###ERROR###\n".$err."\n######",
                    $this->task->nextruntime,
                    round($starttime),
                    round($stoptime),
                    $json->data->exectime,
                    $json->data->dbqueries
                );
            }
        }else{
            // PHP Error, Exception
            $this->endTask(TaskManager::STATUS_FAILED, 
                "3###RETURN CODE: ".$retval."###\n###OUTPUT###\n".$output."\n######\n###ERROR###\n".$err."\n######",
                $this->task->nextruntime,
                round($starttime),
                round($stoptime),
                $stoptime-$starttime,
                0
            );
        }
    }

    function my_shell_exec($cmd, &$stdout=null, &$stderr=null) {
        $proc = proc_open($cmd,[
            1 => ['pipe','w'],
            2 => ['pipe','w'],
        ],$pipes);
        
        $read_output = $read_error = true;
        $buffer_len  = $prev_buffer_len = 0;
        $delay       = 10;
        
        stream_set_blocking($pipes[1], 0);
        stream_set_blocking($pipes[2], 0);
        
        while ($read_error != false or $read_output != false) {
            if ($read_output != false) {
                if(feof($pipes[1])) {
                    fclose($pipes[1]);
                    $read_output = false;
                } else {
                    $str = fgets($pipes[1], 1024);
                    $len = strlen($str);
                    if ($len) {
                        $stdout .= $str;
                        $buffer_len += $len;
                    }
                }
            }
            
            if ($read_error != false) {
                if(feof($pipes[2])) {
                    fclose($pipes[2]);
                    $read_error = false;
                } else {
                    $str = fgets($pipes[2], 1024);
                    $len = strlen($str);
                    if ($len) {
                        $stderr .= $str;
                        $buffer_len += $len;
                    }
                }
            }
            
            if ($buffer_len > $prev_buffer_len) {
                $prev_buffer_len = $buffer_len;
                $delay = 10;
            } else {
                usleep($delay * 1000);
                if ($delay < 160) {
                    $delay = $delay * 2;
                }
            }
        }
        return proc_close($proc);
    }
}

