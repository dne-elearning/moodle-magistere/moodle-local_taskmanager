<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/local/taskmanager/TaskAdhoc.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskScheduled.php');

class TaskManager {

    const GET_TASK_MAX_TRIES = 5;
    const WORKER_LOOP_WAIT_DELAY = 15; // 15 seconds
    const WORKER_SHUTDOWN_HOUR = 0; // midnight
    const LAUNCHER_LOOP_WAIT_DELAY = 15; // 15 seconds
    const LAUNCHER_MAX_LIFESPAN = 604800; // a week
    const ADHOC_COLLECTOR_LOOP_WAIT_DELAY = 15; // 15 seconds
    const ADHOC_MAX_ATTEMPTS = 5;
    const ADHOC_FAIL_DELAY = 10;

    const TYPE_ADHOC = 1;
    const TYPE_SCHEDULED = 2;
    const TYPE_ALL = 3;
    const TYPE_ADHOC_COLLECTOR = 4;
    
    const EVENT_LEVEL_ALERT = 2;
    const EVENT_LEVEL_WARN = 3;
    
    const EVENT_TYPE_FAILED = 2;
    const EVENT_TYPE_CONSECUTIVEFAILED = 3;
    const EVENT_TYPE_NEXTRUNTIME = 4;

    const STATUS_ANY = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 2;
    const STATUS_RUNNING = 3;
    const STATUS_LOCKING = 4;
    const STATUS_WAITING = 5;
    const STATUS_WARNING = 6;

    const WORKER_STATUS_RUNNING = 1;
    const WORKER_STATUS_WAITING = 2;
    const WORKER_STATUS_NOTRESPONDING = 3;
    const WORKER_STATUS_NEW = 4;
    
    const TABLE_EVENT          = 'local_taskmanager_event';
    const TABLE_SCHEDULED      = 'local_taskmanager_scheduled';
    const TABLE_SCHEDULED_LOG  = 'local_taskmanager_sched_log';
    const TABLE_SCHEDULED_LOGS = 'local_taskmanager_sched_logs';
    const TABLE_ADHOC          = 'local_taskmanager_adhoc';
    const TABLE_ADHOC_VM       = 'local_taskmanager_adhoc_vm';
    const TABLE_ADHOC_LOG      = 'local_taskmanager_adhoc_log';
    const TABLE_ADHOC_LOGS     = 'local_taskmanager_adhoc_logs';
    const TABLE_VM             = 'local_taskmanager_vm';
    const TABLE_VM_LOG         = 'local_taskmanager_vm_log';
    const TABLE_WORKER         = 'local_taskmanager_worker';

    const PLUGIN_NAME = 'local_taskmanager';

    private $worker = null;
    private $vm = null;
    private $mariadb103 = false;
    private $logdir = null;
    

    function __construct()
    {
        $this->vm = $this->get_vm(gethostname());

        if ( version_compare($GLOBALS['DB']->get_server_info()['version'], '10.3.0', '>=')){
            $this->mariadb103 = true;
        }
    }
    
    public static function get_status_list() {
        return array(
            'SUCCESS' => self::STATUS_SUCCESS,
            'FAILED '=> self::STATUS_FAILED,
            'RUNNING' => self::STATUS_RUNNING,
            'LOCKING' => self::STATUS_LOCKING,
            'WAITING' => self::STATUS_WAITING,
            'WARNING' => self::STATUS_WARNING,
        );
    }

    public static function get_laststatus_list() {
        return array(
            'SUCCESS' => self::STATUS_SUCCESS,
            'FAILED '=> self::STATUS_FAILED,
            'WARNING' => self::STATUS_WARNING,
        );
    }
    
    function maintenanceEnabled() {
        return get_config(self::PLUGIN_NAME,'maintenance_enabled') === 1;
    }

    function run_tasks() {
        global $DB;
        while(true) {
            $this->l('SEARCHING TASKS');
            $this->worker->lastloop = time();
            $DB->update_record(self::TABLE_WORKER,$this->worker);
            
            if (!$DB->record_exists(self::TABLE_WORKER, array('id'=>$this->worker->id))){
                $this->l('WORKER REMOVED, KILL PROCESS');
                die();
            }
            
            $task = false;
            if ($this->maintenanceEnabled()) {
                $this->l('MAINTENANCE ENABLED, WAITING');
            }else{
                $task = $this->get_nexttask();
            }
            
            if (empty($task)){
                $this->l('No task found');
                $this->l('SLEEPING FOR '.self::WORKER_LOOP_WAIT_DELAY.' seconds');
                sleep(self::WORKER_LOOP_WAIT_DELAY);
            }else{
                $this->worker->lastaction = time();
                $this->worker->totaljobcount++;
                $this->worker->jobcount++;
                $DB->update_record(self::TABLE_WORKER,$this->worker);
                $this->l('Found task : ####'.print_r($task,true).'####');
                $task->execute();
            }
        }
    }

    /**
     * Return next task to be executed by the worker
     */
    function get_nexttask() {
        if ($this->worker->type == self::TYPE_ADHOC || $this->worker->type == self::TYPE_ALL){
            if (get_config(self::PLUGIN_NAME,'adhoc_task_enabled') == 1) {
                $this->l('Check next adhoc task');
                $task = $this->get_next_adhoc_task();
                if (!empty($task)) {
                    return $task;
                }
            }else{
                $this->l('ADHOC TASKS DISABLED');
            }
        }

        if ($this->worker->type == self::TYPE_SCHEDULED || $this->worker->type == self::TYPE_ALL){
            if (get_config(self::PLUGIN_NAME,'schedule_tasks_enabled') != 1) {
                $this->l('SCHEDULED TASKS DISABLED');
                return false;
            }
            $this->l('Check next scheduled task');
            return $this->get_next_scheduled_task();
        }
    }

    function get_next_adhoc_task($try=0) {
        global $DB;
        $DB->execute('SET AUTOCOMMIT = 0');
        if ($this->mariadb103){
            $DB->execute('SET innodb_lock_wait_timeout = 1');
        }else{
            $DB->execute('SET innodb_lock_wait_timeout = 1');
        }
        $query = "SELECT * 
        FROM {".self::TABLE_ADHOC."} 
        WHERE nextruntime < :nextruntime 
        AND status = :status
        AND (vmid IS NULL OR vmid = :vmid)
        LIMIT 1
        FOR UPDATE";
        
        try{
            $record = $DB->get_record_sql($query, array("nextruntime"=> time(), "status"=>self::STATUS_WAITING, "vmid"=>$this->worker->vmid));
        }catch(Exception $e){
            if ($try < self::GET_TASK_MAX_TRIES){
                return $this->get_next_adhoc_task($try+1);
            }
        }

        if ($record !== false) {
            $record->status = self::STATUS_LOCKING;
            $record->workerid = $this->worker->id;
            $DB->update_record(self::TABLE_ADHOC, $record);
        }
        $DB->execute('COMMIT');
        $DB->execute('SET AUTOCOMMIT = 1');

        if ($record === false && $try < self::GET_TASK_MAX_TRIES){
            return $this->get_next_adhoc_task($try+1);
        }

        if ($record === false){
            return false;
        }

        $record->phpbin = $this->vm->phpbin;
        $record->logpath = $this->vm->logpath;
        $record->runvmid = $this->vm->id;

        return new TaskAdhoc($record);
    }


    function get_next_scheduled_task($try=0) {
        global $DB;
        $DB->execute('SET AUTOCOMMIT = 0');
        if ($this->mariadb103){
            $DB->execute('SET innodb_lock_wait_timeout = 0');
        }else{
            $DB->execute('SET innodb_lock_wait_timeout = 1');
        }
        $query = "SELECT ts.*
        FROM {".self::TABLE_SCHEDULED."} ts
        WHERE disabled = 0 
        AND nextruntime < :nextruntime 
        AND status = :status
        AND (vmid IS NULL OR vmid = :vmid)
        LIMIT 1
        FOR UPDATE";
        
        $record = $DB->get_record_sql($query, array("nextruntime"=> time(), "status"=>self::STATUS_WAITING, "vmid"=>$this->worker->vmid));

        if ($record !== false) {
            $record->status = self::STATUS_LOCKING;
            $record->workerid = $this->worker->id;
            $DB->update_record(self::TABLE_SCHEDULED, $record);
        }
        $DB->execute('COMMIT');
        $DB->execute('SET AUTOCOMMIT = 1');

        if ($record === false && $try < self::GET_TASK_MAX_TRIES){
            return $this->get_next_scheduled_task($try+1);
        }

        if ($record === false){
            return false;
        }

        $record->phpbin = $this->vm->phpbin;
        $record->logpath = $this->vm->logpath;
        $record->runvmid = $this->vm->id;

        return new TaskScheduled($record);
    }

    /**
     * MAGISTERE ACAS
     */
    static function get_magistere_acas(){
        $magacaconfig = get_config(self::PLUGIN_NAME,'mag_academy_config');
        if (!file_exists($magacaconfig)){
            echo getmypid().'#'.date('Ymd_His__').'Error: the file "'.$magacaconfig.'" does not exists!'."\n";
            die;
        }

        require_once($magacaconfig);
        return get_magistere_academy_config();
    }

    /**
     * 
     */
    function run_adhoc_collector() {
        global $DB, $CFG;
        $adhoc_lastids = array();
        $acas = self::get_magistere_acas();
        require_once($CFG->dirroot.'/local/taskmanager/multiDB.php');

        while(true) {
            $this->l('ADHOC_COLLECTOR START LOOP');
            $this->worker->lastloop = time();
            $DB->update_record(self::TABLE_WORKER,$this->worker);
            // Get Adhoc vm restrictions
            $adhocvm = array();
            $adhoc_vms = $DB->get_records(self::TABLE_ADHOC_VM);
            foreach($adhoc_vms AS $adhoc_vm) {
                $adhocvm[$adhoc_vm->classname] = $adhoc_vm->vmid;
            }
            $this->l('ADHOC_COLLECTOR FOUND '.count($adhocvm). ' restrictions');

            $aca_adhocmaxids = $DB->get_records_sql('SELECT instance, MAX(taskid) AS maxid FROM {'.self::TABLE_ADHOC.'} GROUP BY instance');

            foreach($aca_adhocmaxids AS $aca_adhocmaxid) {
                $adhoc_lastids[$aca_adhocmaxid->instance] = $aca_adhocmaxid->maxid;
            }

            $taskcount = 0;
            foreach($acas AS $acaname => $acadata) {
                $this->l('Checking aca "'.$acaname.'"');
                if (!isset($adhoc_lastids[$acaname])){
                    $adhoc_lastids[$acaname] = 0;
                }
                
                $tasks = multiDB::instance()->get($acaname)->get_records_sql("SELECT * FROM {task_adhoc} where id > ? ORDER BY id", array($adhoc_lastids[$acaname]));

                foreach($tasks AS $task) {
                    $taskmanager_adhoc = new stdClass();
                    $taskmanager_adhoc->instance = $acaname;
                    $taskmanager_adhoc->taskid = $task->id;
                    $taskmanager_adhoc->classname = $task->classname;
                    $taskmanager_adhoc->nextruntime = $task->nextruntime;
                    $taskmanager_adhoc->customdata = $task->customdata;
                    $taskmanager_adhoc->status = self::STATUS_WAITING;

                    // Check adhoc restrictions
                    if (array_key_exists($task->classname, $adhocvm)) {
                        $taskmanager_adhoc->vmid = $adhocvm[$task->classname];
                    }
                    
                    $insert = $DB->insert_record(self::TABLE_ADHOC, $taskmanager_adhoc, false);

                    $this->worker->lastaction = time();
                    $this->worker->totaljobcount++;
                    $this->worker->jobcount++;
                    $DB->update_record(self::TABLE_WORKER,$this->worker);

                    if (!$insert) {
                        $msg->l('Error: adhoc task insertion failed. ####'.print_r($taskmanager_adhoc,true).'####');
                    }else{
                        $adhoc_lastids[$acaname] = $task->id;
                        $taskcount++;
                    }
                }
                
            }

            $this->l('ADHOC_COLLECTOR '.$taskcount.' new tasks added');
            $this->l('ADHOC_COLLECTOR SLEEPING FOR '.self::ADHOC_COLLECTOR_LOOP_WAIT_DELAY.' seconds');
            sleep(self::ADHOC_COLLECTOR_LOOP_WAIT_DELAY);
        }
    }

    function get_vm($hostname) {
        return $GLOBALS['DB']->get_record(self::TABLE_VM, array('hostname'=>$hostname));
    }

    function get_worker($workerid) {
        return $GLOBALS['DB']->get_record(self::TABLE_WORKER, array('id'=>$workerid));
    }

    /**
     * WORKERS
     */

    function create_worker($hostname, $token, $type) {
        global $DB;
        $vm = $this->get_vm($hostname);

        $worker = new stdClass();
        $worker->vmid = $vm->id;
        $worker->token = $token;
        $worker->type = $type;
        $worker->lastaction = time();
        $worker->status = self::WORKER_STATUS_NEW;
        
        $worker->id = $DB->insert_record(self::TABLE_WORKER, $worker);

        return $worker;
    }

    function setup_worker($workerid) {
        global $DB;

        $this->worker = $this->get_worker($workerid);

        if ($this->worker === false){
            echo 'This worker do not exists!';
            return;
        }
        if ($this->vm->id != $this->worker->vmid){
            echo 'This worker do not belong to this VM';
            return;
        }

        if ($this->worker == false){
            echo 'Error: worker token not found!';
            return;
        }

        $this->worker->pid = getmypid();
        $this->worker->lastloop = time();
        $this->worker->jobcount = 0;
        $this->worker->status = self::WORKER_STATUS_WAITING;
        $DB->update_record(self::TABLE_WORKER, $this->worker);
    }

    function run_worker() {
        if ($this->worker == null){
            return false;
        }

        if ($this->worker->type == self::TYPE_ADHOC_COLLECTOR) {
            $this->run_adhoc_collector();
        }else{
            $this->run_tasks();
        }

    }

    function get_workers($vmid) {
        global $DB;
        return $DB->get_records(self::TABLE_WORKER, array('vmid'=>$vmid));
    }

    function get_worker_log_path($worker) {
        $workerlogpath = $this->vm->logpath.'/'.$worker->name;
        if (!file_exists($workerlogpath)) {
            make_writable_directory($workerlogpath);
        }
        $date = date('Ymd');
        return $workerlogpath.'/'.$date.'.log';
    }



    /**
     * LAUNCHER
     */

    function launch_worker($worker) {
        global $CFG;
        $workerCLIpath = $CFG->dirroot.'/local/taskmanager/worker.php';
        $logPath = $this->get_worker_log_path($worker);
        exec($this->vm->phpbin.' '.$workerCLIpath.' --workerid='.$worker->id.' >> '.$logPath.' 2>&1 &');
    }

    function run_launcher(){
        global $DB;
	$this->vm->starttime = time();
	$this->vm->pid = getmypid();
	$DB->update_record(self::TABLE_VM,$this->vm);

        while(true) {
            $this->lfile('STARTING LAUNCHER LOOP on VM('.$this->vm->id.') : '.$this->vm->hostname);
            $this->vm->lastloop = time();
            $DB->update_record(self::TABLE_VM,$this->vm);

            $workers = $this->get_workers($this->vm->id);
            $this->lfile(count($workers).' workers found for this VM');
            foreach($workers AS $worker){
                if (!file_exists('/proc/'.$worker->pid)) {
                    $this->vm->laststart = time();
                    $DB->update_record(self::TABLE_VM,$this->vm);

                    $this->lfile('Worker pid('.$worker->pid.') not found for the worker(id='.$worker->id.',name="'.$worker->name.'")');
                    $this->lfile('Reset worker old tasks');
                    $DB->execute("UPDATE {".self::TABLE_SCHEDULED."} SET workerid=NULL, status=:status WHERE workerid = :workerid", array('workerid'=>$worker->id, 'status'=>self::STATUS_WAITING));
                    $DB->execute("UPDATE {".self::TABLE_ADHOC."} SET workerid=NULL, status=:status WHERE workerid = :workerid", array('workerid'=>$worker->id, 'status'=>self::STATUS_WAITING));

                    $this->lfile('Launching worker');
                    $this->launch_worker($worker);

                    $log = new stdClass();
                    $log->vmid = $this->vm->id;
                    $log->createdate = time();
                    $log->action = 'startworker';
                    $log->workerid = $worker->id;
                    $DB->insert_record(self::TABLE_VM_LOG, $log);
                }
            }
            $this->lfile('SLEEPING FOR '.self::LAUNCHER_LOOP_WAIT_DELAY.' seconds');
            sleep(self::LAUNCHER_LOOP_WAIT_DELAY);
        }
    }

    function l($msg, $return = true){
        echo getmypid().'#'.date('Ymd_His__').$msg.($return?"\n":"");
    }

    function setlogdir($logdir){
        $this->logdir = $logdir;
    }

    function lfile($msg, $return = true){
        if ($this->logdir === null){
            echo 'ERROR: file not set for lfile()';
            return;
        }
        $out = file_put_contents($this->logdir.'/'.date('Ymd').'.log', getmypid().'#'.date('Ymd_His__').$msg.($return?"\n":""), FILE_APPEND);
        if ($out === false){
            echo 'file put FAILED';
        }
    }
}
