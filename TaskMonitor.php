<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

class TaskMonitor {
    
    function __construct(){}
    
    static function getWorkerCurrentTasks() {
        global $DB;
        $DB->get_record_sql('SELECT * 
FROM (
SELECT ltw.id, ltw.name, ltw.type, ltw.lastloop, ltw.totaljobcount, ltw.jobcount, ltw.status, lts.instance, lts.classname, lts.nextruntime
FROM {'.TaskManager::TABLE_WORKER.'} ltw
LEFT JOIN {'.TaskManager::TABLE_SCHEDULED.'} lts ON (lts.workerid = ltw.id)
WHERE ltw.type = ?
UNION
SELECT ltw.id, ltw.name, ltw.type, ltw.lastloop, ltw.totaljobcount, ltw.jobcount, ltw.status, lta.instance, lta.classname, lta.nextruntime
FROM {'.TaskManager::TABLE_WORKER.'} ltw
LEFT JOIN {'.TaskManager::TABLE_ADHOC.'} lta ON (ltw.id = lta.workerid)
WHERE ltw.type = ?
) wrk
ORDER BY wrk.type, wrk.name', array(TaskManager::TYPE_SCHEDULED,TaskManager::TYPE_ADHOC));
    }
    
    static function get_available_stats(){
        return array(
            'scheduled_success',
            'scheduled_failed',
            'scheduled_any',
            'adhoc_success',
            'adhoc_failed',
            'adhoc_any'
        );
    }
    
    static function get_stats($stattype, $begin, $end, $steps){
        switch ($stattype) {
            case 'scheduled_success':
                return self::get_scheduled_stats($begin, $end, $steps, TaskManager::STATUS_SUCCESS);
                break;
            case 'scheduled_failed':
                return self::get_scheduled_stats($begin, $end, $steps, TaskManager::STATUS_FAILED);
                break;
            case 'scheduled_any':
                return self::get_scheduled_stats($begin, $end, $steps, TaskManager::STATUS_ANY);
                break;
            case 'adhoc_success':
                return self::get_adhoc_stats($begin, $end, $steps, TaskManager::STATUS_SUCCESS);
                break;
            case 'adhoc_failed':
                return self::get_adhoc_stats($begin, $end, $steps, TaskManager::STATUS_FAILED);
                break;
            case 'adhoc_any':
                return self::get_adhoc_stats($begin, $end, $steps, TaskManager::STATUS_ANY);
                break;
            default:
                return false;
        }
    }
    
    static function get_scheduled_stats($begin, $end, $steps, $status = TaskManager::STATUS_ANY) {
        global $DB;
        $stepwidth=floor((($end-$begin)/$steps) / 5) * 5;
        if ($stepwidth<5) {
            $stepwidth=5;
        }
        $params = array('stepwidth'=>$stepwidth, 'stepwidth2'=>$stepwidth, 'begin'=>$begin, 'end'=>$end);
        
        $statussql = '';
        if ($status != TaskManager::STATUS_ANY) {
            $statussql = ' AND status = :status';
            $params['status'] = $status;
        }
        $sql = 'SELECT FLOOR(startdate/:stepwidth)*:stepwidth2 AS "step", COUNT(*) AS "count" FROM {'.TaskManager::TABLE_SCHEDULED_LOG.'} WHERE startdate BETWEEN :begin AND :end'.$statussql.'GROUP BY step';
        return $DB->get_records_sql($sql,$params);
    }
    
    static function get_adhoc_stats($begin, $end, $steps, $status = TaskManager::STATUS_ANY) {
        global $DB;
        $stepwidth=floor((($end-$begin)/$steps) / 5) * 5;
        if ($stepwidth<5) {
            $stepwidth=5;
        }
        $params = array('stepwidth'=>$stepwidth, 'stepwidth2'=>$stepwidth, 'begin'=>$begin, 'end'=>$end);
        
        $statussql = '';
        if ($status != TaskManager::STATUS_ANY) {
            $statussql = ' AND status = :status';
            $params['status'] = $status;
        }
        $sql = 'SELECT FLOOR(startdate/:stepwidth)*:stepwidth2 AS "step", COUNT(*) AS "count" FROM {'.TaskManager::TABLE_ADHOC_LOG.'} WHERE startdate BETWEEN :begin AND :end'.$statussql.' GROUP BY step';
        return $DB->get_records_sql($sql,$params);
    }
    
    static function get_waiting_scheduled(){
        global $DB;
        return $DB->count_records_select(
            TaskManager::TABLE_SCHEDULED, 
            'nextruntime < ? AND status = ? AND disabled = 0', 
            array(time(), TaskManager::STATUS_WAITING)
        );
    }
    
    static function get_todo_adhoc(){
        global $DB;
        return $DB->count_records_select(
            TaskManager::TABLE_ADHOC, 
            'nextruntime < ? AND status = ?', 
            array(time(), TaskManager::STATUS_WAITING)
        );
    }
    
    static function get_failed_adhoc(){
        global $DB;
        return $DB->count_records_select(
            TaskManager::TABLE_ADHOC, 
            'status = ?', 
            array(TaskManager::STATUS_FAILED)
        );
    }
    
    static function get_waiting_adhoc(){
        global $DB;
        return $DB->count_records_select(
            TaskManager::TABLE_ADHOC, 
            'nextruntime >= ? AND status = ?', 
            array(time(), TaskManager::STATUS_WAITING)
        );
    }

    static function l($msg, $return = true) {
        echo date('Ymd_His__').$msg.($return?"\n":"");
    }
}
