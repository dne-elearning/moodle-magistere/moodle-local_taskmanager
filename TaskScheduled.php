<?php

defined('MOODLE_INTERNAL') || die;

class TaskScheduled {
    private $task = null;

    function __construct($record)
    {
        $this->task = $record;
    }

    function startTask() {
        $this->task->status = TaskManager::STATUS_RUNNING;
        $GLOBALS['DB']->update_record(TaskManager::TABLE_SCHEDULED, $this->task);
    }

    function endTask($status, $logs, $nextruntime, $startdate, $enddate, $exectime, $nbqueries) {
        global $DB;
        $scheduled_logs = new stdClass();
        $scheduled_logs->scheduled_id = $this->task->id;
        $scheduled_logs->status = $status;
        $scheduled_logs->nextruntime = $nextruntime;
        $scheduled_logs->startdate = $startdate;
        $scheduled_logs->enddate = $enddate;
        $scheduled_logs->nbqueries = $nbqueries;
        $scheduled_logs->exectime = $exectime;
        $scheduled_logs->vmid = $this->task->runvmid;
        $scheduled_logs->workerid = $this->task->workerid;

        $recordid = $DB->insert_record(TaskManager::TABLE_SCHEDULED_LOG, $scheduled_logs);
        
        $rec_logs = new stdClass();
        $rec_logs->id = $recordid;
        $rec_logs->logs = $logs;
        
        $DB->insert_record_raw(TaskManager::TABLE_SCHEDULED_LOGS, $rec_logs, true, false, true);

        $this->task->status = TaskManager::STATUS_WAITING;
        $this->task->workerid = null;
        $this->task->laststatus = $status;
        $this->task->lastruntime = $startdate;
        $this->task->nextruntime = $this->get_next_scheduled_time($this->task->minute, $this->task->hour, $this->task->day, $this->task->month, $this->task->dayofweek);

        $DB->update_record(TaskManager::TABLE_SCHEDULED, $this->task);
    }

    function execute(){
        $php = $this->task->phpbin;
        $aca = get_config('local_taskmanager', 'mag_academy_path');
        $command = $php.' '.$aca.'/'.$this->task->instance.'/local/taskmanager_agent/run_schedule.php --execute="'.$this->task->classname.'"';
        //echo 'RUNNING CLI COMMAND ##'.$command.'##';

        $this->startTask();
        $starttime = microtime(true);
        $output = $err = '';
        $retval = $this->my_shell_exec($command, $output, $err);
        $stoptime = microtime(true);
        
        echo "OUTPUT CODE: ".$retval."\n";

        // Output look like a valid JSON
        if (!empty($output) && strpos($output,'{"error":') === 0){
            $json = json_decode($output);
            if ($json === null || $json->error == true){
                $this->endTask(TaskManager::STATUS_FAILED, 
                    "2###RETURN CODE: ".$retval."###\n###OUTPUT###\n".$output."\n######\n###ERROR###\n".$err."\n######",
                    $this->task->nextruntime,
                    round($starttime),
                    round($stoptime),
                    $stoptime-$starttime,
                    0
                );
            }else{
                $this->endTask(TaskManager::STATUS_SUCCESS, 
                    "1###RETURN CODE: ".$retval."###\n###OUTPUT###\n".base64_decode($json->data->logs)."\n######\n###ERROR###\n".$err."\n######",
                    $this->task->nextruntime,
                    round($starttime),
                    round($stoptime),
                    $json->data->exectime,
                    $json->data->dbqueries
                );
            }
        }else{
            // PHP Error, Exception
            $this->endTask(TaskManager::STATUS_FAILED, 
                "3###RETURN CODE: ".$retval."###\n###OUTPUT###\n".$output."\n######\n###ERROR###\n".$err."\n######",
                $this->task->nextruntime,
                round($starttime),
                round($stoptime),
                $stoptime-$starttime,
                0
            );
        }
    }

    function my_shell_exec($cmd, &$stdout=null, &$stderr=null) {
        $proc = proc_open($cmd,[
            1 => ['pipe','w'],
            2 => ['pipe','w'],
        ],$pipes);
        
        $read_output = $read_error = true;
        $buffer_len  = $prev_buffer_len = 0;
        $delay       = 10;
        
        stream_set_blocking($pipes[1], 0);
        stream_set_blocking($pipes[2], 0);
        
        while ($read_error != false or $read_output != false) {
            if ($read_output != false) {
                if(feof($pipes[1])) {
                    fclose($pipes[1]);
                    $read_output = false;
                } else {
                    $str = fgets($pipes[1], 1024);
                    $len = strlen($str);
                    if ($len) {
                        $stdout .= $str;
                        $buffer_len += $len;
                    }
                }
            }
            
            if ($read_error != false) {
                if(feof($pipes[2])) {
                    fclose($pipes[2]);
                    $read_error = false;
                } else {
                    $str = fgets($pipes[2], 1024);
                    $len = strlen($str);
                    if ($len) {
                        $stderr .= $str;
                        $buffer_len += $len;
                    }
                }
            }
            
            if ($buffer_len > $prev_buffer_len) {
                $prev_buffer_len = $buffer_len;
                $delay = 10;
            } else {
                usleep($delay * 1000);
                if ($delay < 160) {
                    $delay = $delay * 2;
                }
            }
        }
        return proc_close($proc);
    }

    private static function eval_cron_field($field, $min, $max) {
        // Cleanse the input.
        $field = trim($field);

        // Format for a field is:
        // <fieldlist> := <range>(/<step>)(,<fieldlist>)
        // <step>  := int
        // <range> := <any>|<int>|<min-max>
        // <any>   := *
        // <min-max> := int-int
        // End of format BNF.

        // This function is complicated but is covered by unit tests.
        $range = array();

        $matches = array();
        preg_match_all('@[0-9]+|\*|,|/|-@', $field, $matches);

        $last = 0;
        $inrange = false;
        $instep = false;

        foreach ($matches[0] as $match) {
            if ($match == '*') {
                array_push($range, range($min, $max));
            } else if ($match == '/') {
                $instep = true;
            } else if ($match == '-') {
                $inrange = true;
            } else if (is_numeric($match)) {
                if ($instep) {
                    $i = 0;
                    for ($i = 0; $i < count($range[count($range) - 1]); $i++) {
                        if (($i) % $match != 0) {
                            $range[count($range) - 1][$i] = -1;
                        }
                    }
                    $inrange = false;
                } else if ($inrange) {
                    if (count($range)) {
                        $range[count($range) - 1] = range($last, $match);
                    }
                    $inrange = false;
                } else {
                    if ($match >= $min && $match <= $max) {
                        array_push($range, $match);
                    }
                    $last = $match;
                }
            }
        }

        // Flatten the result.
        $result = array();
        foreach ($range as $r) {
            if (is_array($r)) {
                foreach ($r as $rr) {
                    if ($rr >= $min && $rr <= $max) {
                        $result[$rr] = 1;
                    }
                }
            } else if (is_numeric($r)) {
                if ($r >= $min && $r <= $max) {
                    $result[$r] = 1;
                }
            }
        }
        $result = array_keys($result);
        sort($result, SORT_NUMERIC);
        return $result;
    }

    /**
     * Assuming $list is an ordered list of items, this function returns the item
     * in the list that is greater than or equal to the current value (or 0). If
     * no value is greater than or equal, this will return the first valid item in the list.
     * If list is empty, this function will return 0.
     *
     * @param int $current The current value
     * @param int[] $list The list of valid items.
     * @return int $next.
     */
    private static function next_in_list($current, $list) {
        foreach ($list as $l) {
            if ($l >= $current) {
                return $l;
            }
        }
        if (count($list)) {
            return $list[0];
        }

        return 0;
    }

    /**
     * Calculate when this task should next be run based on the schedule.
     * @return int $nextruntime.
     */
    private static function get_next_scheduled_time($minute, $hour, $day, $month, $dayofweek) {
        global $CFG;

        $validminutes = self::eval_cron_field($minute, 0, 59);
        $validhours = self::eval_cron_field($hour, 0, 23);

        // We need to change to the server timezone before using php date() functions.
        \core_date::set_default_server_timezone();

        $daysinmonth = date("t");
        $validdays = self::eval_cron_field($day, 1, $daysinmonth);
        $validdaysofweek = self::eval_cron_field($dayofweek, 0, 7);
        $validmonths = self::eval_cron_field($month, 1, 12);
        $nextvalidyear = date('Y');

        $currentminute = date("i") + 1;
        $currenthour = date("H");
        $currentday = date("j");
        $currentmonth = date("n");
        $currentdayofweek = date("w");

        $nextvalidminute = self::next_in_list($currentminute, $validminutes);
        if ($nextvalidminute < $currentminute) {
            $currenthour += 1;
        }
        $nextvalidhour = self::next_in_list($currenthour, $validhours);
        if ($nextvalidhour < $currenthour) {
            $currentdayofweek += 1;
            $currentday += 1;
        }
        $nextvaliddayofmonth = self::next_in_list($currentday, $validdays);
        $nextvaliddayofweek = self::next_in_list($currentdayofweek, $validdaysofweek);
        $daysincrementbymonth = $nextvaliddayofmonth - $currentday;
        if ($nextvaliddayofmonth < $currentday) {
            $daysincrementbymonth += $daysinmonth;
        }

        $daysincrementbyweek = $nextvaliddayofweek - $currentdayofweek;
        if ($nextvaliddayofweek < $currentdayofweek) {
            $daysincrementbyweek += 7;
        }

        // Special handling for dayofmonth vs dayofweek:
        // if either field is * - use the other field
        // otherwise - choose the soonest (see man 5 cron).
        if ($dayofweek == '*') {
            $daysincrement = $daysincrementbymonth;
        } else if ($day == '*') {
            $daysincrement = $daysincrementbyweek;
        } else {
            // Take the smaller increment of days by month or week.
            $daysincrement = $daysincrementbymonth;
            if ($daysincrementbyweek < $daysincrementbymonth) {
                $daysincrement = $daysincrementbyweek;
            }
        }

        $nextvaliddayofmonth = $currentday + $daysincrement;
        if ($nextvaliddayofmonth > $daysinmonth) {
            $currentmonth += 1;
            $nextvaliddayofmonth -= $daysinmonth;
        }

        $nextvalidmonth = self::next_in_list($currentmonth, $validmonths);
        if ($nextvalidmonth < $currentmonth) {
            $nextvalidyear += 1;
        }

        // Work out the next valid time.
        $nexttime = mktime($nextvalidhour,
                           $nextvalidminute,
                           0,
                           $nextvalidmonth,
                           $nextvaliddayofmonth,
                           $nextvalidyear);

        return $nexttime;
    }

}

