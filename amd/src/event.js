/* jshint ignore:start */
define(['jquery','jqueryui', 'core/ajax', 'local_taskmanager/jtable'], function($, ui, ajax) {
        
        var str;

        function init(lstr) {
            str = lstr;
            initJtable();
        }
        
        function initJtable() {
            $("#eventtable").jtable({
                title: str.preview_header_title,
                paging: true,
                pageSize: 20,
                pageSizes: [20,30,50,100],
                selecting: false,
                multiselect: false,
                selectingCheckboxes: false,
                sorting: true,
                defaultSorting: "timecreated DESC",
                jqueryuiTheme: true,
                defaultDateFormat: "dd/mm/yy ",
                gotoPageArea: "textbox",
                selectOnRowClick: false,
                actions: {
                    listAction: function (postData, jtParams) {
                        return $.Deferred(function ($dfd) {
                            var postdata = new Object();
                            postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
                            postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
                            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

                            var promises = ajax.call([
                                { methodname: 'local_taskmanager_get_events', args: postdata },
                            ])
                    
                            promises[0].done((response) => {
                                $dfd.resolve(JSON.parse(response.data));
                            });
                        });
                    }
                },
                fields: {
                    id : {
                        type: 'hidden',
                        key: true,
                    },
                    level : {
                        title: str.header_level,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    type: {
                        title: str.header_type,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    tasktype: {
                        title: str.header_tasktype,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    taskid: {
                        title: str.header_taskid,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text'
                    },
                    instance: {
                        title: str.header_instance,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text',
                    },
                    classname: {
                        title: str.header_classname,
                        width: "25%",
                        create: false,
                        edit: false,
                        list: true,
                    },
                    date: {
                        title: str.header_date,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true,
                    },
                    timecreated: {
                        title: str.header_timecreated,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true,
                    }
                }
            });
            
            $('#eventtable').jtable('load');
            
        }
        
        return {
            init : function(str) {
                init(str);
            }
        };

});
