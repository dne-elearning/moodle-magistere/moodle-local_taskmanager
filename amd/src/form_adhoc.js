/* jshint ignore:start */
define(['jquery', 'core/ajax', 'local_taskmanager/jtable', 'local_taskmanager/select2'], function($, ajax) {

    var str;
    var readOnlyMode;
    var jtableSelectedRows = [];

    function init(lstr, readOnly) {
		str = lstr;
        readOnlyMode = readOnly;
        $('.multi-select').select2({
            closeOnSelect : false,
        });
        initJtable();

        $('#multiedit-reprogram').click(handleOnClickReprogram);
        $('#multiedit-delete').click(handleOnClickDelete);
        $('#adhoc-filter').click(handleOnClickFilter);
    }
    
    function initJtable() {
        $("#adhoctable").jtable({
            title: str.preview_header_title,
            paging: true,
            pageSize: 20,
            pageSizes: [20,30,50,100],
            selecting: true,
            multiselect: true,
            selectingCheckboxes: readOnlyMode ? false : true,
            sorting: true,
            defaultSorting: "name ASC",
            jqueryuiTheme: true,
            defaultDateFormat: "dd/mm/yy",
            gotoPageArea: "textbox",
            selectOnRowClick: false,
            actions: {
                listAction: function (postData, jtParams) {
                    return $.Deferred(function ($dfd) {
                        var postdata = formdata();
						postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
						postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
			            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

						var promises = ajax.call([
				            { methodname: 'local_taskmanager_get_adhoctasks', args: postdata },
				        ])
				
				        promises[0].done((response) => {
			    		    $dfd.resolve(JSON.parse(response.data));
				        });
                    });
                }
            },
            fields: {
                id : {
                    type: 'hidden',
                    key: true,
                },
                instance : {
                    title: str.preview_header_instance,
                    width: "25%",
                    create: false,
                    edit: false,
                    list: true
                },
                classname: {
                    title: str.preview_header_classname,
                    width: "25%",
                    create: false,
                    edit: false,
                    list: true
                },
                nextruntime: {
                    title: str.preview_header_nextruntime,
                    width: "15%",
                    create: false,
                    edit: false,
                    list: true,
					type: 'text'
                },
                status: {
                    title: str.preview_header_status,
                    width: "5%",
                    create: false,
                    edit: false,
                    list: true,
                    type: 'text',
                },
                action: readOnlyMode ? undefined : {
                    title: str.preview_header_action,
                    width: "10%",
                    create: false,
                    edit: false,
                    list: true,
                    listClass: 'action-cell',
                    display: (data) => {  
                        var reprogramHtml = '<span class="reprogram-row action" onclick="handleOnClickReprogramRow('+ data.record.id +')"><i class="fa fa-refresh"></i></span>';
                        var deleteHtml = '<span class="delete-row action" onclick="handleOnClickDeleteRow('+ data.record.id +')"><i class="fa fa-trash"></i></span>';
                        return reprogramHtml+deleteHtml;
                    }
                },
            },
            selectionChanged: function () {
                var table = $('#adhoctable');
                var selectedRows = table.jtable('selectedRows');

                // Remove page unselected rows
                var colnew = [];
                for (var i = 0, len = jtableSelectedRows.length; i < len; i++) {
                    row = table.jtable('getRowByKey', jtableSelectedRows[i]);
                    if (!row) {
                        colnew.push(jtableSelectedRows[i]);
                    }
                }
                jtableSelectedRows = colnew;

                // Add newly selected rows
                if (selectedRows.length > 0) {
                    selectedRows.each(function () {
                        var record = $(this).data('record');
                        if (jtableSelectedRows.indexOf(record.id) < 0) {
                            jtableSelectedRows.push(record.id);
                        }
                    });
                }

                // update the row counter 
                $('#nb-selected-rows').text(jtableSelectedRows.length);
            },
            rowInserted: function (event, data) {
                if (jtableSelectedRows.indexOf(data.record.id) >= 0) {
                    $('#adhoctable').jtable('selectRows', data.row);
                }
            }, 
        });
		
		$('#adhoctable').jtable('load');
		
    }

    function handleOnClickFilter() {
        $('#adhoctable').jtable('load');
    }

    
    function handleOnClickDelete() {
        var selectedRows = jtableSelectedRows;
        var ids = [];
        selectedRows.forEach(function(id) {
            ids.push(id);
        });

        if (ids.length > 0) {
            promises = ajax.call([
                {methodname: 'local_taskmanager_multi_delete_adhoc_task', args: {
                    ids:ids,
                }}
            ]);
            promises[0].done(function (response) {
                if (response) {
                    response.ids.forEach(id => {
                        $('#adhoctable').jtable('deleteRecord', {
                            key: id,
                            clientOnly: true,
                            success: () => {
                            },
                        });    
                    }); 
                } else {
                    // TODO
                }
            }).fail(function (ex) {
                console.log(ex);
            });
            return false;
        }
    }

    function handleOnClickReprogram() {
        var selectedRows = jtableSelectedRows;
        var ids = [];
        selectedRows.forEach(function(id) {
            ids.push(id);
        });

        if (ids.length > 0) {
            promises = ajax.call([
                {methodname: 'local_taskmanager_multi_reprogram_adhoc_task', args: {
                    ids:ids,
                }}
            ]);
            promises[0].done(function (response) {
                if (response) {
                    response.tasks.forEach(element => {
                        $('#adhoctable').jtable('updateRecord', {
                            record: {
                                id: element.id,
                                status: element.status,
                            },
                            clientOnly: true,
                            success: () => {
                                $("tr[data-record-key='"+ element.id +"']").addClass('success');
                                setTimeout(()=> {
                                    $("tr[data-record-key='"+ element.id +"']").removeClass('success');
                                }, 3000);
                            },
                        });    
                    }); 
                } else {
                    // TODO
                }
            }).fail(function (ex) {
                console.log(ex);
            });
            return false;
        }
    }

    handleOnClickDeleteRow = (id) => {
        promises = ajax.call([
            {methodname: 'local_taskmanager_multi_delete_adhoc_task', args: {
                ids: [id],
            }}
        ]);
        promises[0].done(function (response) {
            if (response) {
                response.ids.forEach(id => {
                    $('#adhoctable').jtable('deleteRecord', {
                        key: id,
                        clientOnly: true,
                        success: () => {
                        },
                    });    
                }); 
            } else {
                // TODO
            }
        }).fail(function (ex) {
            console.log(ex);
        });
        return false;
    }

    handleOnClickReprogramRow = (id) => {
        promises = ajax.call([
            {methodname: 'local_taskmanager_multi_reprogram_adhoc_task', args: {
                ids:[id],
            }}
        ]);
        promises[0].done(function (response) {
            if (response) {
                response.tasks.forEach(element => {
                    $('#adhoctable').jtable('updateRecord', {
                        record: {
                            id: element.id,
                            status: element.status,
                        },
                        clientOnly: true,
                        success: () => {
                            $("tr[data-record-key='"+ element.id +"']").addClass('success');
                            setTimeout(()=> {
                                $("tr[data-record-key='"+ element.id +"']").removeClass('success');
                            }, 3000);
                        },
                    });    
                }); 
            } else {
                // TODO
            }
        }).fail(function (ex) {
            console.log(ex);
        });
        return false;
    }
    
    function formdata() {
    	var data = new Object();
        data.tasks = $('#tasks-select .multi-select').select2('data').map(val => val.id);
        data.instances = $('#instances-select .multi-select').select2('data').map(val => val.id);
        data.status = $('#status-select .multi-select').select2('data').map(val => val.id);
        return data;
    }
	
	return {
    	init : function(str, readOnlyMode) {
            init(str, readOnlyMode);
        }
    };
});
