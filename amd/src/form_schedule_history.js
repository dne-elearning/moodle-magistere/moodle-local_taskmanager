/* jshint ignore:start */
define(['jquery', 'core/ajax', 'local_taskmanager/jtable'], function($, ajax) {

    var str;

    function init(lstr) {
		str = lstr;
        showpreview();
    }
    
    function showpreview() {
        $("#schedulehistorytable").jtable({
            title: str.preview_header_title,
            paging: true,
            pageSize: 10,
            pageSizes: [10,20,30],
            selecting: false,
            multiselect: false,
            selectingCheckboxes: false,
            sorting: true,
            defaultSorting: "name ASC",
            jqueryuiTheme: true,
            defaultDateFormat: "dd/mm/yy",
            gotoPageArea: "textbox",
            selectOnRowClick: false,
            actions: {
                listAction: function (postData, jtParams) {
                    return $.Deferred(function ($dfd) {
                        var postdata = formdata();
						postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
						postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
			            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

						var promises = ajax.call([
				            { methodname: 'local_taskmanager_get_scheduledtaskshistory', args: postdata },
				        ])
				
				        promises[0].done((response) => {
			    		    $dfd.resolve(JSON.parse(response.data));
				        });
                    });
                }
            },
            fields: {
                instance : {
                    title: str.preview_header_instance,
                    width: "25%",
                    create: false,
                    edit: false,
                    list: true
                },
                classname: {
                    title: str.preview_header_classname,
                    width: "25%",
                    create: false,
                    edit: false,
                    list: true
                },
                component: {
                    title: str.preview_header_component,
                    width: "10%",
                    create: false,
                    edit: false,
                    list: true
                },
                runtime: {
                    title: str.preview_header_runtime,
                    width: "10%",
                    create: false,
                    edit: false,
                    list: true,
					type: 'date'
                },
                duration: {
                    title: str.preview_header_duration,
                    width: "10%",
                    create: false,
                    edit: false,
                    list: true
                },
                result: {
                    title: str.preview_header_result,
                    width: "10%",
                    create: false,
                    edit: false,
                    list: true
                },
                action: {
                    title: str.preview_header_action,
                    width: "10%",
                    create: false,
                    edit: false,
                    list: true
                },
            }
        });
		
		$('#schedulehistorytable').jtable('load');
		
    }
    
    function formdata() {
    	var data = new Object();
    	// data.tasks = $('#f_tasks').val();
    	// data.instances = $('#f_instances').val();

        data.tasks = [];
    	data.instances = [];
    	return data;
    }
	
	return {
    	init : function(str) {
            console.log('I\'m called');
            init(str);
        }
    };
});
