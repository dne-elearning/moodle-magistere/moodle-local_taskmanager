/* jshint ignore:start */
define(['jquery','jqueryui', 'core/ajax', 'local_taskmanager/jtable', 'local_taskmanager/select2'], function($, ui, ajax) {
    //define('bootstrap-multiselect', function() {
        
        var str;
        var readOnlyMode;
        const text_field_size = 6;
        const text_field_max_size = 25;
        var jtableSelectedRows = [];

        function init(lstr, readOnly) {
            str = lstr;
            readOnlyMode = readOnly;
            $('.multi-select').select2({
                closeOnSelect : false,
            });
            initJtable();

            $('#multiedit-editform').hide();
            $('#scheduled-filter').click(handleOnClickFilter);
            $('#multiedit-reset').click(handleOnClickReset);
            $('#multiedit-disable').click(handleOnClickDisable);
            $('#multiedit-enable').click(handleOnClickEnable);
            $('#multiedit-edit').click(handleOnClickEdit);
            $('#multiedit-validate-edit').click(handleOnClickValidateEdit);
        }
        
        function createHtmlId(id, field) {
            return id + '-' + field;
        }

        resetNextRuntime = (id) => {
            promises = ajax.call([
                {methodname: 'local_taskmanager_multi_reset_st_nextruntime', args: {
                    ids:[id],
                }}
            ]);
            promises[0].done(function (response) {
                if (response) {
                    response.tasks.forEach(element => {
                        $('#scheduletable').jtable('updateRecord', {
                            record: {
                                id: element.id,
                                nextruntime: element.nextruntime,
                            },
                            clientOnly: true,
                            success: () => {
                                $("tr[data-record-key='"+ element.id +"']").addClass('success');
                                setTimeout(()=> {
                                    $("tr[data-record-key='"+ element.id +"']").removeClass('success');
                                }, 3000);
                            },
                        });    
                    }); 
                } else {
                    // TODO
                }
            }).fail(function (ex) {
                console.log(ex);
            });
            return false;
        }
        
        function initJtable() {
            var disabled = readOnlyMode ? 'disabled' : '';
            $("#scheduletable").jtable({
                title: str.preview_header_title,
                paging: true,
                pageSize: 20,
                pageSizes: [20,30,50,100],
                selecting: true,
                multiselect: true,
                selectingCheckboxes: readOnlyMode ? false : true,
                sorting: true,
                defaultSorting: "name ASC",
                jqueryuiTheme: true,
                defaultDateFormat: "dd/mm/yy ",
                gotoPageArea: "textbox",
                selectOnRowClick: false,
                actions: {
                    listAction: function (postData, jtParams) {
                        return $.Deferred(function ($dfd) {
                            var postdata = formdata();
                            postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
                            postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
                            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

                            var promises = ajax.call([
                                { methodname: 'local_taskmanager_get_scheduledtasks', args: postdata },
                            ])
                    
                            promises[0].done((response) => {
                                $dfd.resolve(JSON.parse(response.data));
                            });
                        });
                    }
                },
                fields: {
                    id : {
                        type: 'hidden',
                        key: true,
                    },
                    instance : {
                        title: str.preview_header_instance,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    classname: {
                        title: str.preview_header_classname,
                        width: "20%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    component: {
                        title: str.preview_header_component,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    lastruntime: {
                        title: str.preview_header_lastruntime,
                        width: "18%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text'
                    },
                    nextruntime: {
                        title: str.preview_header_nextruntime,
                        width: "18%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text',
                    },
                    minute: {
                        title: str.preview_header_minute,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false,
                        display: (data) => {
                            var name = 'minute';
                            var id = createHtmlId(data.record.id, name);
                            return '<input '+ disabled +' size="'+ text_field_size +'" maxlength="'+ text_field_max_size +'" id="'+ id +'" name="' + name + '" value="'+ data.record.minute +'" />';
                        }
                    },
                    hour: {
                        title: str.preview_header_hour,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false,
                        display: (data) => {
                            var name = 'hour';
                            var id = createHtmlId(data.record.id, name);
                            return '<input '+ disabled +' size="'+ text_field_size +'" maxlength="'+ text_field_max_size +'" id="'+ id +'" name="' + name + '" value="'+ data.record.hour +'" />';
                        }
                    },
                    day: {
                        title: str.preview_header_day,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false,
                        display: (data) => {
                            var name = 'day';
                            var id = createHtmlId(data.record.id, name);
                            return '<input '+ disabled +' size="'+ text_field_size +'" maxlength="'+ text_field_max_size +'" id="'+ id +'" name="' + name + '" value="'+ data.record.day +'" />';
                        }
                    },
                    month: {
                        title: str.preview_header_month,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false,
                        display: (data) => {
                            var name = 'month';
                            var id = createHtmlId(data.record.id, name);
                            return '<input '+ disabled +' size="'+ text_field_size +'" maxlength="'+ text_field_max_size +'" id="'+ id +'" name="' + name + '" value="'+ data.record.month +'" />';
                        }
                    },
                    dayofweek: {
                        title: str.preview_header_dayofweek,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false,
                        display: (data) => {
                            var name = 'dayofweek';
                            var id = createHtmlId(data.record.id, name);
                            return '<input '+ disabled +' size="'+ text_field_size +'" maxlength="'+ text_field_max_size +'" id="'+ id +'" name="' + name + '" value="'+ data.record.dayofweek +'" />';
                        }
                    },
                    disabled: {
                        title: str.preview_header_enabled,
                        width: "2%",
                        create: false,
                        edit: false,
                        list: true,
                        display: (data) => {
                            var name = 'disabled';
                            var id = createHtmlId(data.record.id, name);
                            return '<input ' + disabled + ' type="checkbox" id="'+ id +'" name="' + name + '" '+ (data.record.disabled == "0" ? 'checked' : '') + '/>';
                        }
                    },
                    vm: {
                        title: str.preview_header_vm,
                        width: "15%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false,
                        display: (data) => {
                            return data.record.vm_select;
                        }
                    },
                    status: {
                        title: str.preview_header_status,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    laststatus: {
                        title: str.preview_header_laststatus,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                    },
                    action: readOnlyMode ? undefined : {
                        title: str.preview_header_action,
                        width: "8%",
                        create: false,
                        edit: false,
                        list: true,
                        listClass: 'action-cell',
                        sorting: false,
                        display: (data) => {  
                            $resetNextRuntimeHtml = '<span class="reset-runtime action" onclick="resetNextRuntime('+ data.record.id +')"><i class="fa fa-undo"></i></span>';
                            $updateRowHtml = '<span class="update-row action" onclick="handleOnEditRow('+ data.record.id +')"><i class="fa fa-floppy-o"></i></span>';
                            return $resetNextRuntimeHtml+$updateRowHtml;
                        }
                    }
                },
                selectionChanged: function () {
                	var table = $('#scheduletable');
                    var selectedRows = table.jtable('selectedRows');

                    // Remove page unselected rows
                    var colnew = [];
                    for (var i = 0, len = jtableSelectedRows.length; i < len; i++) {
                        row = table.jtable('getRowByKey', jtableSelectedRows[i]);
                        if (!row) {
                            colnew.push(jtableSelectedRows[i]);
                        }
                    }
                    jtableSelectedRows = colnew;

                    // Add newly selected rows
                    if (selectedRows.length > 0) {
                        selectedRows.each(function () {
                            var record = $(this).data('record');
                            if (jtableSelectedRows.indexOf(record.id) < 0) {
                            	jtableSelectedRows.push(record.id);
                            }
                        });
                    }

                    // update the row counter 
                    $('#nb-selected-rows').text(jtableSelectedRows.length);
                },
                rowInserted: function (event, data) {
                	if (jtableSelectedRows.indexOf(data.record.id) >= 0) {
                		$('#scheduletable').jtable('selectRows', data.row);
                    }
                },
            });
            
            $('#scheduletable').jtable('load');
            
        }

        function handleOnClickFilter() {
            $('#scheduletable').jtable('load');
        }

        function handleOnClickReset() {
            var selectedRows = jtableSelectedRows;
            var ids = [];
            selectedRows.forEach(function(id) {
                ids.push(id);
            });

            if (ids.length > 0) {
                promises = ajax.call([
                    {methodname: 'local_taskmanager_multi_reset_st_nextruntime', args: {
                        ids:ids,
                    }}
                ]);
                promises[0].done(function (response) {
                    if (response) {
                        response.tasks.forEach(element => {
                            $('#scheduletable').jtable('updateRecord', {
                                record: {
                                    id: element.id,
                                    nextruntime: element.nextruntime,
                                },
                                clientOnly: true,
                                success: () => {
                                    $("tr[data-record-key='"+ element.id +"']").addClass('success');
                                    setTimeout(()=> {
                                        $("tr[data-record-key='"+ element.id +"']").removeClass('success');
                                    }, 3000);
                                },
                            });    
                        }); 
                    } else {
                        // TODO
                    }
                }).fail(function (ex) {
                    console.log(ex);
                });
                return false;
            }
        }

        function handleOnClickDisable() {
            var selectedRows = jtableSelectedRows;
            var ids = [];
            selectedRows.forEach(function(id) {
                ids.push(id);
            });

            if (ids.length > 0) {
                promises = ajax.call([
                    {methodname: 'local_taskmanager_multi_disable_st', args: {
                        ids:ids,
                    }}
                ]);
                promises[0].done(function (response) {
                    if (response) {
                        response.tasks.forEach(element => {
                            $('#scheduletable').jtable('updateRecord', {
                                record: {
                                    id: element.id,
                                    disabled: element.disabled,
                                },
                                clientOnly: true,
                                success: () => {
                                    $("tr[data-record-key='"+ element.id +"']").addClass('success');
                                    setTimeout(()=> {
                                        $("tr[data-record-key='"+ element.id +"']").removeClass('success');
                                    }, 3000);
                                },
                            });    
                        }); 
                    } else {
                        // TODO
                    }
                }).fail(function (ex) {
                    console.log(ex);
                });
                return false;
            }
        }

        function handleOnClickEnable() {
            var selectedRows = jtableSelectedRows;
            var ids = [];
            selectedRows.forEach(function(id) {
                ids.push(id);
            });

            if (ids.length > 0) {
                promises = ajax.call([
                    {methodname: 'local_taskmanager_multi_enable_st', args: {
                        ids:ids,
                    }}
                ]);
                promises[0].done(function (response) {
                    if (response) {
                        response.tasks.forEach(element => {
                            $('#scheduletable').jtable('updateRecord', {
                                record: {
                                    id: element.id,
                                    disabled: element.disabled,
                                },
                                clientOnly: true,
                                success: () => {
                                    $("tr[data-record-key='"+ element.id +"']").addClass('success');
                                    setTimeout(()=> {
                                        $("tr[data-record-key='"+ element.id +"']").removeClass('success');
                                    }, 3000);
                                },
                            });    
                        }); 
                    } else {
                        // TODO
                    }
                }).fail(function (ex) {
                    console.log(ex);
                });
                return false;
            }
        }

        handleOnEditRow = (id) => {
            minute = $('#' + createHtmlId(id,'minute')).val();
            hour = $('#' + createHtmlId(id,'hour')).val();
            day = $('#' + createHtmlId(id,'day')).val();
            month = $('#' + createHtmlId(id,'month')).val();
            dayofweek = $('#' + createHtmlId(id,'dayofweek')).val();
            disabled = !$('#' + createHtmlId(id,'disabled')).is(':checked');
            vmid = $('#' + createHtmlId(id,'vmid')).val();

            promises = ajax.call([
                {methodname: 'local_taskmanager_update_st_rows', args: {
                    ids: [id],
                    minute: minute,
                    hour: hour,
                    day: day,
                    month: month,
                    dayofweek: dayofweek,
                    disabled: disabled,
                    vmid: vmid,
                }}
            ]);
            promises[0].done(function (response) {
                if (response) {
                    response.tasks.forEach((task) => {
                        $('#scheduletable').jtable('updateRecord', {
                            record: {
                                id: task.id,
                                minute: task.minute,
                                hour: task.hour,
                                day: task.day,
                                month: task.month,
                                dayofweek: task.dayofweek,
                                disabled: task.disabled,
                                vm_select: task.vm_select,
                            },
                            clientOnly: true,
                            success: () => {
                                $("tr[data-record-key='"+ task.id +"']").addClass('success');
                                setTimeout(()=> {
                                    $("tr[data-record-key='"+ task.id +"']").removeClass('success');
                                }, 3000);
                            },
                        });
                    })
                } else {
                    // TODO
                }
            }).fail(function (ex) {
                console.log(ex);
            });
            return false;
        }

        function handleOnClickEdit() {
            $('#multiedit-editform').show();
        }

        function handleOnClickValidateEdit() {
            minute = $('#multi-edit-minute').val();
            hour = $('#multi-edit-hour').val();
            day = $('#multi-edit-day').val();
            month = $('#multi-edit-month').val();
            dayofweek = $('#multi-edit-dayofweek').val(); 
            // send the data
            
            promises = ajax.call([
                {methodname: 'local_taskmanager_update_st_rows', args: {
                    ids: jtableSelectedRows,
                    minute: minute !== '' ? minute : null,
                    hour: hour !== '' ? hour : null,
                    day: day !== '' ? day : null,
                    month: month !== '' ? month : null,
                    dayofweek: dayofweek !== '' ? dayofweek : null,
                }}
            ]);
            promises[0].done(function (response) {
                if (response) {
                    response.tasks.forEach((task) => {
                        $('#scheduletable').jtable('updateRecord', {
                            record: {
                                id: task.id,
                                minute: task.minute,
                                hour: task.hour,
                                day: task.day,
                                month: task.month,
                                dayofweek: task.dayofweek,
                                disabled: task.disabled,
                                vm_select: task.vm_select,
                            },
                            clientOnly: true,
                            success: () => {
                                $('#multiedit-editform').hide();
                                $("tr[data-record-key='"+ task.id +"']").addClass('success');
                                setTimeout(()=> {
                                    $("tr[data-record-key='"+ task.id +"']").removeClass('success');
                                }, 3000);
                            },
                        });
                    })
                } else {
                    // TODO
                }
            }).fail(function (ex) {
                console.log(ex);
            });
            return false;

        }
        
        function formdata() {
            var data = new Object();
            data.tasks = $('#tasks-select .multi-select').select2('data').map(val => val.id);
            data.instances = $('#instances-select .multi-select').select2('data').map(val => val.id);
            data.laststatus = $('#status-select .multi-select').select2('data').map(val => val.id);
            return data;
        }
        
        return {
            init : function(str, readOnlyMode) {
                init(str, readOnlyMode);
            }
        };

});
