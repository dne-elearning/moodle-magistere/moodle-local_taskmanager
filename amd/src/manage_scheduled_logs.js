/* jshint ignore:start */
define(['jquery','jqueryui', 'core/ajax', 'local_taskmanager/jtable', 'local_taskmanager/select2'], function($, ui, ajax) {
    //define('bootstrap-multiselect', function() {
        
        var str;
        var readOnlyMode;

        function init(lstr, readOnly) {
            str = lstr;
            readOnlyMode = readOnly;
            $('.multi-select').select2({
                closeOnSelect : false,
            });
            initJtable();

            $('#scheduled-logs-filter').click(handleOnClickFilter);
        }
        
        function createHtmlId(id, field) {
            return id + '-' + field;
        }


        function initJtable() {
            $("#scheduledlogstable").jtable({
                title: str.preview_header_title,
                paging: true,
                pageSize: 20,
                pageSizes: [20,30,50,100],
                selecting: false,
                multiselect: false,
                selectingCheckboxes: false,
                sorting: true,
                defaultSorting: "enddate DESC",
                jqueryuiTheme: true,
                defaultDateFormat: "dd/mm/yy ",
                gotoPageArea: "textbox",
                selectOnRowClick: false,
                actions: {
                    listAction: function (postData, jtParams) {
                        return $.Deferred(function ($dfd) {
                            var postdata = formdata();
                            postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
                            postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
                            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

                            var promises = ajax.call([
                                { methodname: 'local_taskmanager_get_scheduledtaskslogs', args: postdata },
                            ])
                    
                            promises[0].done((response) => {
                                $dfd.resolve(JSON.parse(response.data));
                            });
                        });
                    }
                },
                fields: {
                    instance : {
                        title: str.preview_header_instance,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    classname: {
                        title: str.preview_header_classname,
                        width: "20%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    component: {
                        title: str.preview_header_component,
                        width: "10%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    nextruntime: {
                        title: str.preview_header_nextruntime,
                        width: "18%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text',
                    },
                    startdate: {
                        title: str.preview_header_startdate,
                        width: "18%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text',
                    },
                    enddate: {
                        title: str.preview_header_enddate,
                        width: "18%",
                        create: false,
                        edit: false,
                        list: true,
                        type: 'text',
                    },
                    status: {
                        title: str.preview_header_status,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    nbqueries: {
                        title: str.preview_header_nbqueries,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    exectime: {
                        title: str.preview_header_exectime,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    vmid: {
                        title: str.preview_header_vm,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    workerid: {
                        title: str.preview_header_worker,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true
                    },
                    logs: readOnlyMode ? undefined :{
                        title: str.preview_header_logs,
                        width: "5%",
                        create: false,
                        edit: false,
                        list: true,
                        sorting: false
                    },
                }
            });
            
            //$('#scheduledlogstable').jtable('load');
            
        }

        function handleOnClickFilter() {
            $('#scheduledlogstable').jtable('load');
        }
        
        function formdata() {
            var data = new Object();

            data.instances = $('#instances-select .multi-select').select2('data').map(val => val.id);
            data.tasks = $('#tasks-select .multi-select').select2('data').map(val => val.id);
            data.status = $('#status-select .multi-select').select2('data').map(val => val.id);
            
            return data;
        }
        
        return {
            init : function(str, readOnlyMode) {
                init(str, readOnlyMode);
            }
        };

});
