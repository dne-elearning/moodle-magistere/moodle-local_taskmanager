<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines a scheduled task to synchronized the scheduled tasks of each platforms.
 *
 * @package    local_taskmanager
 * @copyright  2023 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_taskmanager\task;

defined('MOODLE_INTERNAL') || die();

class archive_scheduled_logs extends \core\task\scheduled_task {
    
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('archive_scheduled_logs', 'local_taskmanager');
    }
    
    /**
     * Execute the scheduled task.
     */
    public function execute() {
        global $DB;
        
        $this->l('ARCHIVE LOGS');
        
        $delay = 50; // days
        
        $this->l('Cleaning log table with records older than '.$delay.' days.');
        
        $time = mktime(0,0,0,date('m'),date('d')-$delay,date('Y'));
        
        $this->l('Moving all records before '.date('Y-m-d H:i:s',$time));
        
        $transaction = $DB->start_delegated_transaction();
        $countlog2 = $DB->count_records('local_taskmanager_sched_log2');
        $countlog = $DB->count_records_select('local_taskmanager_sched_log', 'startdate < ?', array($time));
        
        $this->l('Scheduled logs archive table have '.$countlog2.' records, we found '.$countlog.' records to move');
        
        $DB->execute('INSERT INTO {local_taskmanager_sched_log2} SELECT * FROM {local_taskmanager_sched_log} WHERE startdate < ?', array($time));
        
        $countlog2_2 = $DB->count_records('local_taskmanager_sched_log2');
        
        $this->l('found '.$countlog2_2.' records in scheduled logs archive table after moving');
        
        if ($countlog2 + $countlog != $countlog2_2) {
            $this->l('The logs counts do not match, the value is '.$countlog2_2.' instead of '.($countlog2 + $countlog).'. We rollback the transaction');
            $transaction->rollback(new \Exception('Logs archive count do not match!'));
        }else{
            $DB->execute('DELETE FROM {local_taskmanager_sched_log} WHERE startdate < ?', array($time));
            $transaction->allow_commit();
        }
        
        $this->l('END ARCHIVE LOGS');
    }

    function l($msg, $return = true){
        echo date('Ymd_His__').$msg.($return?"\n":"");
    }
}

