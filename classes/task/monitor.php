<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines a scheduled task to synchronized the scheduled tasks of each platforms.
 *
 * @package    local_taskmanager
 * @copyright  2023 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_taskmanager\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

class monitor extends \core\task\scheduled_task {
    
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('monitor_task', 'local_taskmanager');
    }
    
    /**
     * Execute the scheduled task.
     */
    public function execute() {
        global $DB;
        
        
        
        $this->l('MONITOR');
        
        $alertdelay = get_config('local_taskmanager', 'alert_delay');
        $warndelay  = get_config('local_taskmanager', 'warn_delay');
        
        $alert_nextrun_delay = get_config('local_taskmanager', 'alert_nextrun_threshold');
        $warn_nextrun_delay = get_config('local_taskmanager', 'warn_nextrun_delay');
        
        $alert_failed_threshold = get_config('local_taskmanager', 'alert_failed_threshold');
        $alert_consecutivefailed_threshold = get_config('local_taskmanager', 'alert_consecutivefailed_threshold');
        
        $warn_failed_threshold = get_config('local_taskmanager', 'warn_failed_threshold');
        $warn_consecutivefailed_threshold = get_config('local_taskmanager', 'warn_consecutivefailed_threshold');
        
        if ($alertdelay == false || 
            $warndelay == false ||
            $alert_nextrun_delay == false ||
            $alert_failed_threshold == false || 
            $alert_consecutivefailed_threshold == false ||
            $warn_nextrun_delay == false ||
            $warn_failed_threshold == false || 
            $warn_consecutivefailed_threshold == false){
                $this->l('Configuration is not set!');
                return false;
        }
        
        $now = time();
        
        $alertnextruntime = $now-$alert_nextrun_delay;
        $warnnextruntime = $now-$warn_nextrun_delay;
        $alerttime = $now-$alertdelay;
        $warntime = $now-$warndelay;
        
        $scheduled = $DB->get_records_select(\TaskManager::TABLE_SCHEDULED, 'disabled = ? AND lastwarn < ?', array(0,$warntime));
        
        $this->l('Found '.count($scheduled).' tasks');
        foreach ($scheduled AS $s){
            $laststatus = $DB->get_records(\TaskManager::TABLE_SCHEDULED_LOG, array('scheduled_id'=>$s->id), 'id DESC', 'id, status, startdate', 0, 20);
            $this->l('aaa '.count($laststatus));
            //print_r($laststatus);
            $lastrun = null;
            $hassucceed = false;
            $success =  $failed = $consecutivefailed = $other = 0;
            foreach ($laststatus AS $ls){
                if ($lastrun==null){
                    $lastrun = $ls;
                }
                if ($ls->status == \TaskManager::STATUS_FAILED){
                    if (!$hassucceed){
                        $consecutivefailed++;
                    }
                    $failed++;
                }else if ($ls->status == \TaskManager::STATUS_SUCCESS){
                    $success++;
                    $hassucceed = true;
                }else{
                    $other++;
                }
            }
            
            if ($s->lastalert < $alerttime && $consecutivefailed > $alert_consecutivefailed_threshold){
                $this->add_alert_event(
                    \TaskManager::EVENT_LEVEL_ALERT,
                    \TaskManager::EVENT_TYPE_CONSECUTIVEFAILED,
                    \TaskManager::TYPE_SCHEDULED,
                    $s,
                    $lastrun->startdate
                );
            }else if ($s->lastalert < $alerttime && $failed > $alert_failed_threshold){
                $this->add_alert_event(
                    \TaskManager::EVENT_LEVEL_ALERT,
                    \TaskManager::EVENT_TYPE_FAILED,
                    \TaskManager::TYPE_SCHEDULED,
                    $s,
                    $lastrun->startdate
                );
            }else if ($consecutivefailed > $warn_consecutivefailed_threshold){
                $this->add_alert_event(
                    \TaskManager::EVENT_LEVEL_WARN,
                    \TaskManager::EVENT_TYPE_CONSECUTIVEFAILED,
                    \TaskManager::TYPE_SCHEDULED,
                    $s,
                    $lastrun->startdate
                );
            }else if ($failed > $warn_failed_threshold){
                $this->add_alert_event(
                    \TaskManager::EVENT_LEVEL_WARN,
                    \TaskManager::EVENT_TYPE_FAILED,
                    \TaskManager::TYPE_SCHEDULED,
                    $s,
                    $lastrun->startdate
                );
            }
            
            if ($s->nextruntime < $alertnextruntime){
                $this->add_alert_event(
                    \TaskManager::EVENT_LEVEL_ALERT,
                    \TaskManager::EVENT_TYPE_NEXTRUNTIME,
                    \TaskManager::TYPE_SCHEDULED,
                    $s,
                    $lastrun->startdate
                );
            }else if ($s->nextruntime < $warnnextruntime){
                $this->add_alert_event(
                    \TaskManager::EVENT_LEVEL_WARN,
                    \TaskManager::EVENT_TYPE_NEXTRUNTIME,
                    \TaskManager::TYPE_SCHEDULED,
                    $s,
                    $lastrun->startdate
                );
            }
            
        }
        
        
        $adhoctime=$now-3600;
        
        $adhocs = $DB->get_records_select(\TaskManager::TABLE_ADHOC, 'status = ? AND attempts = ? AND nextruntime > ?', array(\TaskManager::STATUS_FAILED,5,$adhoctime));
        
        $failedadhoc = count($adhocs);
        
        
        $events = $DB->get_records_select(\TaskManager::TABLE_EVENT, 'timecreated > ?', array($now));
        
        $counters = array();
        foreach ($events AS $event){
            $eventid = $event->tasktype.'_'.$event->level.'_'.$event->type;
            if (!isset($counters[$eventid])){$counters[$eventid]=0;}
            $counters[$eventid]++;
        }
        
        $alert_text = '';
        $warn_text = '';
        $alert_count = 0;
        $warn_count = 0;
        
        if ($failedadhoc>0){
            $alert_text .= '- '.$failedadhoc.' tâches adhoc ont échouées trop de fois cette dernière heure'."\n";
            $alert_count+=$failedadhoc;
        }
        
        if (isset($counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_FAILED]) &&
            $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_FAILED] > 0){
                $count = $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_FAILED];
                $alert_text .= '- '.$count.' tâches ont dépasser le seuil d\'échecs'."\n";
                $alert_count+=$count;
        }
        if (isset($counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_CONSECUTIVEFAILED]) &&
            $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_CONSECUTIVEFAILED] > 0){
                $count = $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_CONSECUTIVEFAILED];
                $alert_text .= '- '.$count.' tâches ont dépassée la limite d\'échec consécutifs'."\n";
                $alert_count+=$count;
        }
        if (isset($counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_NEXTRUNTIME]) &&
            $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_NEXTRUNTIME] > 0){
                $count = $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_ALERT.'_'.\TaskManager::EVENT_TYPE_NEXTRUNTIME];
                $alert_text .= '- '.$count.' tâches sont significativement en retard d\'exécution'."\n";
                $alert_count+=$count;
        }
        
        if (isset($counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_FAILED]) &&
            $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_FAILED] > 0){
                $count = $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_FAILED];
                $warn_text .= '- '.$count.' tâches ont dépasser le seuil d\'échecs'."\n";
                $warn_count+=$count;
        }
        if (isset($counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_CONSECUTIVEFAILED]) &&
            $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_CONSECUTIVEFAILED] > 0){
                $count = $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_CONSECUTIVEFAILED];
                $warn_text .= '- '.$count.' tâches ont dépassée la limite d\'échec consécutifs'."\n";
                $warn_count+=$count;
        }
        if (isset($counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_NEXTRUNTIME]) &&
            $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_NEXTRUNTIME] > 0){
                $count = $counters[\TaskManager::TYPE_SCHEDULED.'_'.\TaskManager::EVENT_LEVEL_WARN.'_'.\TaskManager::EVENT_TYPE_NEXTRUNTIME];
                $warn_text .= '- '.$count.' tâches sont significativement en retard d\'exécution'."\n";
                $warn_count+=$count;
        }
        
        $this->l('Results: '.$warn_count.' warn and '.$alert_count.' alert');
        $this->l('WARN: ####'.$warn_text.'####');
        $this->l('ALERT: ####'.$alert_text.'####');
        
        
        $warn_users = get_users_by_capability(\context_system::instance(), 'local/taskmanager:receive_warn');
        $this->l('Found '.count($warn_users).' user to warn');
        
        if (!empty($alert_text)){
            
            $users = get_users_by_capability(\context_system::instance(), 'local/taskmanager:receive_alert');
            
            $this->l('Found '.count($users).' user to alert');
            
            $subject = '[M@gistère] Gestionnaire de tâches, '.$alert_count.' alertes';
            
            $message = $alert_count.' problèmes ont été identifiés sur certaines tâches :'."\n\n".$alert_text;
            
            $messagehtml = $alert_count.' problèmes ont été identifiés sur certaines tâches :'."<br>\n<br>\n".nl2br($alert_text);
            
            $messageshort = 'Gestionnaire de tâches: '.$alert_count.' alertes';
            
            foreach ($users AS $user){
                $this->l('Sending alert to user '.$user->id.' (username='.$user->username.'#email='.$user->email.')');
                if (array_key_exists($users->id,$warn_users)){
                    continue;
                }
                
                $this->send_message('alert', $user, $subject, $message, $messagehtml, $messageshort);
            }
        }
        
        if (!empty($alert_text) || !empty($warn_text)){
            
            $subject = '[M@gistère] Gestionnaire de tâches, '.$alert_count.' alertes et '.$warn_count.' warn';
            
            $message = $alert_count.' alertes et '.$warn_count.' warn ont été identifiés sur certaines tâches :'."\n\n".$alert_text.'\n\n'.$warn_text;
            
            $messagehtml = $alert_count.' alertes et '.$warn_count.' warn ont été identifiés sur certaines tâches :'."<br>\n<br>\n".nl2br($alert_text)."<br>\n<br>\n".nl2br($warn_text);
            
            $messageshort = 'Gestionnaire de tâches: '.$alert_count.' alertes et '.$warn_count.' warn';
            
            foreach ($warn_users AS $user){
                $this->l('Sending warn to user '.$user->id.' (username='.$user->username.'#email='.$user->email.')');
                
                $this->send_message('warn', $user, $subject, $message, $messagehtml, $messageshort);
            }
            
        }
        
        /*
        <FIELD NAME="id"          TYPE="int"  LENGTH="10"  NOTNULL="true" SEQUENCE="true"/>
        <FIELD NAME="level"       TYPE="int"  LENGTH="10"  NOTNULL="true" DEFAULT="0"/>
        <FIELD NAME="type"        TYPE="int"  LENGTH="10"  NOTNULL="true" DEFAULT="0"/>
        <FIELD NAME="tasktype"    TYPE="int"  LENGTH="10"  NOTNULL="true" DEFAULT="0"/>
        <FIELD NAME="taskid"      TYPE="int"  LENGTH="10"  NOTNULL="true" DEFAULT="none"/>
        <FIELD NAME="instance"    TYPE="char" LENGTH="50"  NOTNULL="true" DEFAULT="0"/>
        <FIELD NAME="classname"   TYPE="char" LENGTH="255" NOTNULL="true" DEFAULT="0"/>
        <FIELD NAME="timecreated" TYPE="int"  LENGTH="4"   NOTNULL="true" DEFAULT="0"/>
        */
        
        
        
        
        $this->l('END MONITOR');
    }
    
    function add_alert_event($eventlevel, $eventtype, $tasktype, $task, $date) {
        global $DB;
        
        $now = time();
        
        $data = new \stdClass();
        $data->level = $eventlevel;
        $data->type = $eventtype;
        $data->tasktype = $tasktype;
        $data->taskid = $task->id;
        $data->instance = $task->instance;
        $data->classname = $task->classname;
        $data->date = $date;
        $data->timecreated = $now;
        
        $DB->insert_record(\TaskManager::TABLE_EVENT, $data);
        
        $taskupdate = new \stdClass();
        $taskupdate->id = $task->id;
        
        if ($eventlevel ==  \TaskManager::EVENT_LEVEL_ALERT){
            $taskupdate->lastalert = $now;
        }
        $taskupdate->lastwarn = $now;
        
        $DB->update_record(\TaskManager::TABLE_SCHEDULED, $taskupdate);
    }
    
    function send_message($type, $userto, $subject, $message, $messagehtml, $messageshort){
        $eventdata = new \core\message\message();
        $eventdata->component           = 'local_taskmanager';
        $eventdata->name                = $type;
        $eventdata->userfrom            = \core_user::get_noreply_user();
        $eventdata->userto              = $userto;
        $eventdata->subject             = $subject;
        $eventdata->fullmessage         = $message;
        $eventdata->fullmessageformat   = FORMAT_PLAIN;
        $eventdata->fullmessagehtml     = $messagehtml;
        $eventdata->smallmessage        = $messageshort;
        $eventdata->notification        = 1;
        
        
        return message_send($eventdata);
    }

    function l($msg, $return = true){
        echo date('Ymd_His__').$msg.($return?"\n":"");
    }
}

