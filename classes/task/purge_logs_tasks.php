<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines a scheduled task to synchronized the scheduled tasks of each platforms.
 *
 * @package    local_taskmanager
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_taskmanager\task;

defined('MOODLE_INTERNAL') || die();

class purge_logs_tasks extends \core\task\scheduled_task {
    
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('synchronise_scheduled_tasks', 'local_taskmanager');
    }
    
    /**
     * Execute the scheduled task.
     */
    public function execute() {
        global $DB, $CFG;
        require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');
        
        $delay = 2678400;
        $threshold = time() - $delay;
        
        $this->l('Purge threshold '.date('Y-m-d H:i:s',$threshold));
        $this->l('Purge scheduled logs');
        $DB->execute('DELETE FROM {'.\TaskManager::TABLE_SCHEDULED_LOGS.'} WHERE id IN(SELECT tsl.id FROM {'.\TaskManager::TABLE_SCHEDULED_LOG.'} tsl WHERE tsl.startdate < ?)', array($threshold));
        
        $this->l('Purge adhoc logs');
        $DB->execute('DELETE FROM {'.\TaskManager::TABLE_ADHOC_LOGS.'} WHERE id IN(SELECT tal.id FROM {'.\TaskManager::TABLE_ADHOC_LOG.'} tal WHERE tal.startdate < ?)', array($threshold));
    }

    function l($msg, $return = true){
        echo date('Ymd_His__').$msg.($return?"\n":"");
    }
}
