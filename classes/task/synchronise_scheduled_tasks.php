<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines a scheduled task to synchronized the scheduled tasks of each platforms.
 *
 * @package    local_taskmanager
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_taskmanager\task;

defined('MOODLE_INTERNAL') || die();

class synchronise_scheduled_tasks extends \core\task\scheduled_task {
    
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('synchronise_scheduled_tasks', 'local_taskmanager');
    }
    
    /**
     * Execute the scheduled task.
     */
    public function execute() {
        global $DB, $CFG;
        require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');
        require_once($CFG->dirroot.'/local/taskmanager/multiDB.php');
        
        $acas = \TaskManager::get_magistere_acas();
        $this->l('SCHEDULED TASK SYNCHRONIZER START LOOP');
        foreach($acas AS $acaname => $acadata) {
            $this->l('-- Start aca '.$acaname);
            $knownTasksName = $DB->get_records_sql("SELECT classname FROM {".\TaskManager::TABLE_SCHEDULED."} WHERE instance = ?", array($acaname));
            $instanceTasksName = \multiDB::instance()->get($acaname)->get_records_sql("SELECT classname FROM {task_scheduled}");

            $allTaskName = array_merge(array_keys($knownTasksName), array_keys($instanceTasksName));

            foreach($allTaskName AS $taskName) {
                if (in_array($taskName, array_keys($knownTasksName)) && !in_array($taskName, array_keys($instanceTasksName))) {
                    // delete the task from admin 
                    $this->l("---- Deleting task ".$taskName." for instance " .$acaname);
                    $DB->delete_records(\TaskManager::TABLE_SCHEDULED, array('classname'=> $taskName, 'instance' => $acaname));
                } elseif (!in_array($taskName, array_keys($knownTasksName)) && in_array($taskName, array_keys($instanceTasksName))) {
                    $instanceTask = \multiDB::instance()->get($acaname)->get_record('task_scheduled', array('classname' => $taskName));

                    $task = new \stdClass();
                    $task->instance = $acaname;
                    $task->component = $instanceTask->component;
                    $task->classname = $instanceTask->classname;
                    $task->disabled = $instanceTask->disabled;
                    $task->lastruntime = $instanceTask->lastruntime;
                    $task->nextruntime = $instanceTask->nextruntime;
                    $task->minute = $instanceTask->minute;
                    $task->hour = $instanceTask->hour;
                    $task->day = $instanceTask->day;
                    $task->month = $instanceTask->month;
                    $task->dayofweek = $instanceTask->dayofweek;
                    $task->status = \TaskManager::STATUS_WAITING;

                    try {
                        $this->l("---- Inserting new task ".$task->classname." with data :\n ##### \n ".print_r($task,true)."\n ##### ");
                        $DB->insert_record(\TaskManager::TABLE_SCHEDULED, $task);
                    } catch(\Exception $e) {
                        $this->l("---- Error : insert failed for task ".$task->classname." : \n ### ".$e->getMessage()."\n".$e->getTraceAsString()."\n #####");
                    }
                }
            }
        }
    }

    function l($msg, $return = true){
        echo date('Ymd_His__').$msg.($return?"\n":"");
    }
}

