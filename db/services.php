<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_taskmanager external functions and service definitions.
 *
 * @package    local_taskmanager
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$services = array(
    'local_taskmanager_service' => array(
        'functions' => array(
            'local_taskmanager_get_scheduledtasks',
            'local_taskmanager_get_adhoctasks',
            'local_taskmanager_get_scheduledtaskslogs',
            'local_taskmanager_get_adhoctaskslogs',
            'local_taskmanager_multi_reset_st_nextruntime',
            'local_taskmanager_multi_disable_st',
            'local_taskmanager_multi_enable_st',
            'local_taskmanager_update_st_rows',
            'local_taskmanager_multi_reprogram_adhoc_task',
            'local_taskmanager_multi_delete_adhoc_task',
            'local_taskmanager_get_stats',
            'local_taskmanager_get_events'
        ),
        'requiredcapability' => '',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' =>  'local_taskmanager_service',
        'downloadfiles' => 0,
        'uploadfiles'  => 0,
    )
);

$functions = array(
    'local_taskmanager_get_scheduledtasks' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'get_scheduledtasks',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Get scheduled tasks',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_get_adhoctasks' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'get_adhoctasks',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Get adhoc tasks',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_get_scheduledtaskslogs' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'get_scheduledtaskslogs',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Get scheduled tasks logs',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_get_adhoctaskslogs' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'get_adhoctaskslogs',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Get adhoc tasks logs',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_multi_reset_st_nextruntime' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'multi_reset_st_nextruntime',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Reset runtime for multiple rows',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_multi_disable_st' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'multi_disable_st',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Disable multiple rows',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_multi_enable_st' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'multi_enable_st',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Enable multiple rows',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_update_st_rows' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'update_st_rows',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Edit multiple rows at once',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_multi_reprogram_adhoc_task' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'multi_reprogram_adhoc_task',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Reprogram multiple adhoc tasks at once',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_multi_delete_adhoc_task' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'multi_delete_adhoc_task',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'Delete multiple adhoc tasks at once',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_get_stats' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'get_stats',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'get stats',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'local_taskmanager_get_events' => array(
        'classname' => 'local_taskmanager_external',
        'methodname' => 'get_events',
        'classpath' => 'local/taskmanager/externallib.php',
        'description' => 'get events',
        'type' => 'read',
        'capabilities' => 'mod/taskmanager:view_event',
        'ajax' => true,
    ),
);


