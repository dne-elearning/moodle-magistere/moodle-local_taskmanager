<?php
/**
 * This file keeps track of upgrades to
 * the centraladmin module
 *
 * Sometimes, changes between versions involve
 * alterations to database structures and other
 * major things that may break installations.
 *
 * The upgrade function in this file will attempt
 * to perform all the necessary actions to upgrade
 * your older installation to the current version.
 *
 * If there's something it cannot do itself, it
 * will tell you what you need to do.
 *
 * The commands in here will all be database-neutral,
 * using the methods of database_manager class
 *
 * Please do not forget to use upgrade_set_timeout()
 * before any action that may take longer time to finish.
 *
 * @package   local_taskmanager
 * @copyright 2021 TCS
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_taskmanager_upgrade($oldversion) {
    global $DB;
    $dbman = $DB->get_manager();
    
    
    if ($oldversion < 2022031800) {
        $table = new xmldb_table('local_taskmanager_sched_logs');
        
        // Adding fields to table local_taskmanager_sched_logs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('logs', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        // Conditionally launch create table for local_taskmanager_sched_logs.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        $table = new xmldb_table('local_taskmanager_adhoc_logs');
        
        // Adding fields to table local_taskmanager_adhoc_logs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('logs', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        // Conditionally launch create table for local_taskmanager_adhoc_logs.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Data savepoint reached.
        upgrade_plugin_savepoint(true, 2022031800, 'local', 'taskmanager');
    }
    
    if ($oldversion < 2023010100) {
        $table = new xmldb_table('local_taskmanager_adhoc_log');
        
        $field = new xmldb_field('taskid', XMLDB_TYPE_INTEGER, '10', null, true, null, '0', 'instance');
        
        if (!$dbman->field_exists($table, $field)){
            $dbman->add_field($table, $field);
        }
        
        // Data savepoint reached.
        upgrade_plugin_savepoint(true, 2023010100, 'local', 'taskmanager');
    }
    
    if ($oldversion < 2023010200) {
        $table = new xmldb_table('local_taskmanager_adhoc');
        
        $field = new xmldb_field('lastwarn', XMLDB_TYPE_INTEGER, '10', null, true, null, '0');
        
        if (!$dbman->field_exists($table, $field)){
            $dbman->add_field($table, $field);
        }
        
        $field = new xmldb_field('lastalert', XMLDB_TYPE_INTEGER, '10', null, true, null, '0');
        
        if (!$dbman->field_exists($table, $field)){
            $dbman->add_field($table, $field);
        }
        
        $table = new xmldb_table('local_taskmanager_scheduled');
        
        $field = new xmldb_field('lastwarn', XMLDB_TYPE_INTEGER, '10', null, true, null, '0');
        
        if (!$dbman->field_exists($table, $field)){
            $dbman->add_field($table, $field);
        }
        
        $field = new xmldb_field('lastalert', XMLDB_TYPE_INTEGER, '10', null, true, null, '0');
        
        if (!$dbman->field_exists($table, $field)){
            $dbman->add_field($table, $field);
        }
        
        upgrade_plugin_savepoint(true, 2023010200, 'local', 'taskmanager');
    }
    
    if ($oldversion < 2023010201) {
        $table = new xmldb_table('local_taskmanager_event');
        
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('level', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('tasktype', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('taskid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('instance', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('classname', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('date', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        upgrade_plugin_savepoint(true, 2023010201, 'local', 'taskmanager');
    }
    
    return true;
}
