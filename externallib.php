<?php

require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskMonitor.php');

class local_taskmanager_external extends external_api {

    public const PLUGIN_NAME = 'local_taskmanager';

    public const DATE_TIME_FORMAT = 'd-m-Y H:i:s';
    
    private static function create_vm_select($taskid, $vmid) {
        global $DB, $USER;
        
        $vmlist = $DB->get_records_sql('SELECT tmv.* FROM {'.TaskManager::TABLE_VM.'} tmv INNER JOIN {'.TaskManager::TABLE_WORKER.'} tmw ON tmw.vmid = tmv.id WHERE tmw.type IN (?,?)', array(
            \TaskManager::TYPE_ALL,
            \TaskManager::TYPE_SCHEDULED,
        ));

        $disabled = '';
        if ($USER->id != 2) {
            $disabled = 'disabled';
        }
        $select_vm = '<select '.$disabled.' class="vm-select" id="'.$taskid.'-vmid">';
        $select_vm .= '<option value="0" '.($vmid ? '' : 'selected').' >'.get_string('any', self::PLUGIN_NAME).'</option>';
        foreach($vmlist as $vm) {

            if ($pos = strpos($vm->hostname, '.')){
                $hostname = substr($vm->hostname, 0, $pos);
            } else {
                $hostname = substr($vm->hostname, 0);
            }
            
            $select_vm .= '<option value="'.$vm->id.'" '.($vmid == $vm->id ? 'selected' : '').' >'.$hostname.'</option>';
        }
        $select_vm .= '</select>';
        return $select_vm;
    }

    public static function get_scheduledtasks_parameters() {
        return new external_function_parameters(
            array(
                'tasks'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'instances'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'laststatus' => new external_multiple_structure(new external_value(PARAM_RAW)),
                'si' => new external_value(PARAM_INT),
                'ps' => new external_value(PARAM_INT),
                'so' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_scheduledtasks_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_scheduledtasks($tasks, $instances, $laststatus, $si, $ps, $so)
    {
        global $DB;
      
        $order = '';
        if (!empty($so)){
            $order = ' ORDER BY '.$so;
        }
        
        $limit = ' LIMIT '.$si.','.$ps;

        $whereClause= '';
        $andNeeded = false;
        $params = array();
        if (($tasks && count($tasks) > 0) || ($instances && count($instances) > 0) || ($laststatus && count($laststatus) > 0)) {
            $whereClause = ' WHERE ';    
        }
        if ($tasks && count($tasks) > 0) {
            list($tasksInSql, $tasksInParams) = $DB->get_in_or_equal($tasks, SQL_PARAMS_NAMED, 'task');
            $whereClause.= 'classname '.$tasksInSql;
            $andNeeded = true;
            $params = array_merge($params, $tasksInParams);
        }
        if ($instances && count($instances) > 0) {
            list($instancesInSql, $instancesInParams) = $DB->get_in_or_equal($instances, SQL_PARAMS_NAMED, 'instance');
            $whereClause.= ($andNeeded ? ' AND' : '').' instance '.$instancesInSql;
            $andNeeded = true;
            $params = array_merge($params, $instancesInParams);
        }
        if ($laststatus && count($laststatus) > 0) {
            list($statusInSql, $statusInParams) = $DB->get_in_or_equal($laststatus, SQL_PARAMS_NAMED, 'laststatus');
            $whereClause.= ($andNeeded ? ' AND' : '').' laststatus '.$statusInSql;
            $params = array_merge($params, $statusInParams);
        }

        $query ='SELECT SQL_CALC_FOUND_ROWS id, instance, component, classname, disabled,  lastruntime, nextruntime, minute, hour, day, month, dayofweek, status, laststatus, vmid FROM {'.TaskManager::TABLE_SCHEDULED.'}'.$whereClause.$order.$limit;
        $records = $DB->get_records_sql($query, $params);
        
        $recordCount = $DB->get_record_sql("SELECT FOUND_ROWS() AS found_rows")->found_rows;
        foreach($records AS $record) {
            $record->lastruntime = date(self::DATE_TIME_FORMAT, $record->lastruntime);
            $record->nextruntime = date(self::DATE_TIME_FORMAT, $record->nextruntime);

            // laststatus
            $statusList = array_flip(TaskManager::get_status_list());
            $classVal = null;
            if ($record->laststatus == TaskManager::STATUS_FAILED) {
                $classVal = 'error';
            } else if ($record->laststatus == TaskManager::STATUS_SUCCESS) {
                $classVal = 'success';
            } else if ($record->laststatus == TaskManager::STATUS_WARNING) {
                $classVal = 'warning';
            }
            $class = $classVal ? 'class="'.$classVal.'"' : '';
            if ($record->laststatus) {
                $record->laststatus = '<span '.$class.'>'.$statusList[$record->laststatus].'</span>'; 
            }


            $record->status = $statusList[$record->status];
            $record->vm_select = self::create_vm_select($record->id,$record->vmid);
        }

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = array_values($records);
        
        return array('data'=>json_encode($jTableResult));
    }

    public static function get_adhoctasks_parameters() {
        return new external_function_parameters(
            array(
                'tasks'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'instances'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'status' => new external_multiple_structure(new external_value(PARAM_RAW)),
                'si' => new external_value(PARAM_INT),
                'ps' => new external_value(PARAM_INT),
                'so' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_adhoctasks_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_adhoctasks($tasks, $instances, $status, $si, $ps, $so)
    {
        global $DB;
      
        $order = '';
        if (!empty($so)){
            $order = ' ORDER BY '.$so;
        }
        
        $limit = ' LIMIT '.$si.','.$ps;

        $whereClause= '';
        $andNeeded = false;
        $params = array();
        if (($tasks && count($tasks) > 0) || ($instances && count($instances) > 0) || ($status && count($status) > 0)) {
            $whereClause = ' WHERE ';    
        }
        if ($tasks && count($tasks) > 0) {
            list($tasksInSql, $tasksInParams) = $DB->get_in_or_equal($tasks, SQL_PARAMS_NAMED, 'task');
            $whereClause.= 'classname '.$tasksInSql;
            $andNeeded = true;
            $params = array_merge($params, $tasksInParams);
        }
        if ($instances && count($instances) > 0) {
            list($instancesInSql, $instancesInParams) = $DB->get_in_or_equal($instances, SQL_PARAMS_NAMED, 'instance');
            $whereClause.= ($andNeeded ? ' AND' : '').' instance '.$instancesInSql;
            $andNeeded = true;
            $params = array_merge($params, $instancesInParams);
        }
        if ($status && count($status) > 0) {
            list($statusInSql, $statusInParams) = $DB->get_in_or_equal($status, SQL_PARAMS_NAMED, 'status');
            $whereClause.= ($andNeeded ? ' AND' : '').' status '.$statusInSql;
            $params = array_merge($params, $statusInParams);
        }

        $query ='SELECT SQL_CALC_FOUND_ROWS id, instance, taskid, classname, nextruntime, customdata, status, vmid, attempts FROM {'.TaskManager::TABLE_ADHOC.'}'.$whereClause.$order.$limit;
        $records = $DB->get_records_sql($query, $params);
        
        $recordCount = $DB->get_record_sql("SELECT FOUND_ROWS() AS found_rows")->found_rows;
        
        foreach($records AS $record) {
            $record->nextruntime = date(self::DATE_TIME_FORMAT, $record->nextruntime);

            // laststatus
            $statusList = array_flip(TaskManager::get_status_list());
            $classVal = null;
            if ($record->status == TaskManager::STATUS_FAILED) {
                $classVal = 'error';
            } else if ($record->status == TaskManager::STATUS_SUCCESS) {
                $classVal = 'success';
            } else if ($record->status == TaskManager::STATUS_WARNING) {
                $classVal = 'warning';
            }
            $class = $classVal ? 'class="'.$classVal.'"' : '';
            $record->status = '<span '.$class.'>'.$statusList[$record->status].'</span>'; 


            // $record->status = $statusList[$record->status];
            //$record->vm_select = self::create_vm_select($record->id,$record->vmid);
        }

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = array_values($records);
        
        return array('data'=>json_encode($jTableResult));
    }
    


    public static function get_scheduledtaskslogs_parameters() {
        return new external_function_parameters(
            array(
                'tasks'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'instances'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'status' => new external_multiple_structure(new external_value(PARAM_RAW)),
                'si' => new external_value(PARAM_INT),
                'ps' => new external_value(PARAM_INT),
                'so' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_scheduledtaskslogs_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_scheduledtaskslogs($tasks, $instances, $status, $si, $ps, $so)
    {
        global $DB;
        
        $order = '';
        if (!empty($so)){
            if (strpos($so,'instance') === 0 || strpos($so,'component') === 0 || strpos($so,'classname') === 0) {
                $so = 's.'.$so;
            }else{
                $so = 'sl.'.$so;
            }
            $order = ' ORDER BY '.$so;
        }
        
        $limit = ' LIMIT '.$si.','.$ps;
        
        $whereClause= '';
        $params = array();
        if (count($tasks) > 0) {
            list($tasksInSql, $tasksInParams) = $DB->get_in_or_equal($tasks, SQL_PARAMS_NAMED, 'task');
            $whereClause .= ' AND s.classname '.$tasksInSql;
            $params = array_merge($params, $tasksInParams);
        }
        if (count($instances) > 0) {
            list($instancesInSql, $instancesInParams) = $DB->get_in_or_equal($instances, SQL_PARAMS_NAMED, 'instance');
            $whereClause .= ' AND s.instance '.$instancesInSql;
            $params = array_merge($params, $instancesInParams);
        }
        if (count($status) > 0) {
            list($statusInSql, $statusInParams) = $DB->get_in_or_equal($status, SQL_PARAMS_NAMED, 'status');
            $whereClause .= ' AND sl.status '.$statusInSql;
            $params = array_merge($params, $statusInParams);
        }
        
        $query = 'SELECT SQL_CALC_FOUND_ROWS 
sl.id, s.instance, s.component, s.classname, sl.status, sl.nextruntime, sl.startdate, sl.enddate, sl.nbqueries, sl.exectime, sl.vmid, sl.workerid 
FROM {'.TaskManager::TABLE_SCHEDULED_LOG.'} sl
INNER JOIN {'.TaskManager::TABLE_SCHEDULED.'} s ON (s.id = sl.scheduled_id)
WHERE 1'.$whereClause.$order.$limit;
        //echo '####'.$query.'####';
        $records = $DB->get_records_sql($query, $params);
        
        $recordCount = $DB->get_record_sql("SELECT FOUND_ROWS() AS found_rows")->found_rows;
        
        
        foreach($records AS $record) {
            
            $log_url = new moodle_url('/local/taskmanager/manage/scheduled_logs_log.php', array('id'=>$record->id));
            unset($record->id);
            $record->nextruntime = date(self::DATE_TIME_FORMAT, $record->nextruntime);
            $record->startdate = date(self::DATE_TIME_FORMAT, $record->startdate);
            $record->enddate = date(self::DATE_TIME_FORMAT, $record->enddate);
            $record->logs = '<a href="'.$log_url->out().'" target="_blank"><i class="icon fa fa-clipboard"></i></a>';
            
            $record->status = strtoupper(get_string('status_'.$record->status, 'local_taskmanager'));
        }
        
        $records = array_values($records);
        
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $records;
        
        return array('data'=>json_encode($jTableResult));
    }

    
    
    
    public static function get_adhoctaskslogs_parameters() {
        return new external_function_parameters(
            array(
                'tasks'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'instances'  => new external_multiple_structure(new external_value(PARAM_RAW)),
                'status' => new external_multiple_structure(new external_value(PARAM_RAW)),
                'si' => new external_value(PARAM_INT),
                'ps' => new external_value(PARAM_INT),
                'so' => new external_value(PARAM_RAW)
            )
            );
    }
    
    public static function get_adhoctaskslogs_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW)
            )
            );
    }
    
    public static function get_adhoctaskslogs($tasks, $instances, $status, $si, $ps, $so)
    {
        global $DB;
        
        $order = '';
        if (!empty($so)){
            $order = ' ORDER BY '.$so;
        }
        
        $limit = ' LIMIT '.$si.','.$ps;
        
        $whereClause= '';
        $params = array();
        if (count($tasks) > 0) {
            list($tasksInSql, $tasksInParams) = $DB->get_in_or_equal($tasks, SQL_PARAMS_NAMED, 'task');
            $whereClause .= ' AND classname '.$tasksInSql;
            $params = array_merge($params, $tasksInParams);
        }
        if (count($instances) > 0) {
            list($instancesInSql, $instancesInParams) = $DB->get_in_or_equal($instances, SQL_PARAMS_NAMED, 'instance');
            $whereClause .= ' AND instance '.$instancesInSql;
            $params = array_merge($params, $instancesInParams);
        }
        if (count($status) > 0) {
            list($statusInSql, $statusInParams) = $DB->get_in_or_equal($status, SQL_PARAMS_NAMED, 'status');
            $whereClause .= ' AND status '.$statusInSql;
            $params = array_merge($params, $statusInParams);
        }
        
        $query = 'SELECT SQL_CALC_FOUND_ROWS
id, instance, classname, status, nextruntime, startdate, enddate, nbqueries, exectime, vmid, workerid
FROM {'.TaskManager::TABLE_ADHOC_LOG.'} WHERE 1'.$whereClause.$order.$limit;
        //echo '####'.$query.'####';
        $records = $DB->get_records_sql($query, $params);
        
        $recordCount = $DB->get_record_sql("SELECT FOUND_ROWS() AS found_rows")->found_rows;
        
        
        foreach($records AS $record) {
            
            $log_url = new moodle_url('/local/taskmanager/manage/adhoc_logs_log.php', array('id'=>$record->id));
            unset($record->id);
            $record->nextruntime = date(self::DATE_TIME_FORMAT, $record->nextruntime);
            $record->startdate = date(self::DATE_TIME_FORMAT, $record->startdate);
            $record->enddate = date(self::DATE_TIME_FORMAT, $record->enddate);
            $record->logs = '<a href="'.$log_url->out().'" target="_blank"><i class="icon fa fa-clipboard"></i></a>';
            
            $record->status = strtoupper(get_string('status_'.$record->status, 'local_taskmanager'));
        }
        
        $records = array_values($records);
        
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $records;
        
        return array('data'=>json_encode($jTableResult));
    }

    public static function get_scheduledtasks_names_parameters() {
        return new external_function_parameters(
            array(
            )
        );
    }
    
    public static function get_scheduledtasks_names_returns() {
        return new external_single_structure(
            array(
                'names' => new external_multiple_structure(
                    new external_value(PARAM_RAW)
                )
            )
        );
    }

    public static function get_scheduledtasks_names() {
        return array_keys( $GLOBALS['DB']->get_records_sql('SELECT classname FROM {'.TaskManager::TABLE_SCHEDULED.'} GROUP BY classname') );
    }


    public static function get_scheduledtasks_instances_parameters() {
        return new external_function_parameters(
            array(
            )
        );
    }
    
    public static function get_scheduledtasks_instances_returns() {
        return new external_single_structure(
            array(
                'instances' => new external_multiple_structure(
                    new external_value(PARAM_RAW)
                )
            )
        );
    }

    public static function get_scheduledtasks_instances() {
        return array_keys( $GLOBALS['DB']->get_records_sql('SELECT instance FROM {'.TaskManager::TABLE_SCHEDULED.'} GROUP BY instance') );
    }

    public static function multi_reset_st_nextruntime_parameters() {
        return new external_function_parameters(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
            )
        );
    }
    
    public static function multi_reset_st_nextruntime_returns() {
        return new external_single_structure(
            array(
                'tasks' => new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT),
                        'nextruntime' => new external_value(PARAM_TEXT)
                        )
                    )
                )
            )
        );
    }

    public static function multi_reset_st_nextruntime($ids) {
        global $DB, $USER;

        if ($USER->id != 2) {
            throw new Exception('Only user admin can edit the data.');
        }

        $newTime = time();
        
        list($insql, $inparams) = $DB->get_in_or_equal($ids);
        $DB->set_field_select(TaskManager::TABLE_SCHEDULED, 'nextruntime', $newTime, 'id '.$insql, $inparams);
        $tasks = array();
        $newDate = date(self::DATE_TIME_FORMAT,$newTime);
        foreach($ids as $id) {
            $tasks[] = array(
                'id' => $id,
                'nextruntime' => $newDate,
            );
        }

        return array(
            'tasks' => $tasks,
        );
    }

    public static function multi_disable_st_parameters() {
        return new external_function_parameters(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
            )
        );
    }
    
    public static function multi_disable_st_returns() {
        return new external_single_structure(
            array(
                'tasks' => new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT),
                        'disabled' => new external_value(PARAM_TEXT)
                        )
                    )
                )
            )
        );
    }


    public static function multi_disable_st($ids) {
        global $DB, $USER;

        if ($USER->id != 2) {
            throw new Exception('Only user admin can edit the data.');
        }

        return self::multi_disable_toggle_st($ids, true);
    }

    public static function multi_enable_st_parameters() {
        return new external_function_parameters(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
            )
        );
    }
    
    public static function multi_enable_st_returns() {
        return new external_single_structure(
            array(
                'tasks' => new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT),
                        'disabled' => new external_value(PARAM_BOOL)
                        )
                    )
                )
            )
        );
    }

    public static function multi_enable_st($ids) {
        return self::multi_disable_toggle_st($ids, false);
    }

    private static function multi_disable_toggle_st($ids, $disabled) {
        global $DB, $USER;

        if ($USER->id != 2) {
            throw new Exception('Only user admin can edit the data.');
        }

        list($insql, $inparams) = $DB->get_in_or_equal($ids);
        $DB->set_field_select(TaskManager::TABLE_SCHEDULED, 'disabled', $disabled, 'id '.$insql, $inparams);
        $tasks = array();
        foreach($ids as $id) {
            $tasks[] = array(
                'id' => $id,
                'disabled' => $disabled,
            );
        }

        return array(
            'tasks' => $tasks,
        );
    }

    public static function update_st_rows_parameters() {
        return new external_function_parameters(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
                'minute' => new external_value(PARAM_TEXT, '', VALUE_OPTIONAL),
                'hour' => new external_value(PARAM_TEXT, '', VALUE_OPTIONAL),
                'day' => new external_value(PARAM_TEXT, '', VALUE_OPTIONAL),
                'month' => new external_value(PARAM_TEXT, '', VALUE_OPTIONAL),
                'dayofweek' => new external_value(PARAM_TEXT, '', VALUE_OPTIONAL),
                'disabled' => new external_value(PARAM_BOOL, '', VALUE_OPTIONAL),
                'vmid' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
            )
        );
    }
    
    public static function update_st_rows_returns() {
        return new external_single_structure(
            array(
                'tasks' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT),
                            'minute' => new external_value(PARAM_TEXT),
                            'hour' => new external_value(PARAM_TEXT),
                            'day' => new external_value(PARAM_TEXT),
                            'month' => new external_value(PARAM_TEXT),
                            'dayofweek' => new external_value(PARAM_TEXT),
                            'disabled' => new external_value(PARAM_TEXT),
                            'vmid' => new external_value(PARAM_INT),
                            'vm_select' => new external_value(PARAM_RAW),
                        )
                    )
                )
            )
        );
    }

    public static function update_st_rows($ids, $minute = null, $hour = null, $day = null, $month = null, $dayofweek = null, $disabled = null, $vmid = null) {
        global $DB, $USER;

        if ($USER->id != 2) {
            throw new Exception('Only user admin can edit the data.');
        }

        $tasks = array();

        foreach($ids as $id) {
            $record = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('id' => $id));
            if ($record) {

                if (isset($minute)) $record->minute = trim($minute);
                if (isset($hour)) $record->hour = trim($hour);
                if (isset($day)) $record->day = trim($day);
                if (isset($month)) $record->month = trim($month);
                if (isset($dayofweek)) $record->dayofweek = trim($dayofweek);
                if (isset($disabled)) $record->disabled = $disabled ? '1' : '0';
                if (isset($vmid)) $record->vmid = ($vmid == 0) ? null : $vmid;

                $DB->update_record(TaskManager::TABLE_SCHEDULED, $record);

                $record->vm_select = self::create_vm_select($record->id,$record->vmid);

                $tasks[] = array(
                    'id' => $id, 
                    'minute'=> $record->minute, 
                    'hour'=>  $record->hour, 
                    'day' => $record->day, 
                    'month'=>  $record->month, 
                    'dayofweek'=> $record->dayofweek, 
                    'disabled' => $record->disabled, 
                    'vmid' => $record->vmid,
                    'vm_select' => $record->vm_select,
                );

            } else {
                throw new Exception('local_taskmanager_scheduled record with id '.$id.' not found');
            }
        }
        
        return array(
            'tasks' => $tasks,
        );
    }

    public static function multi_reprogram_adhoc_task_parameters() {
        return new external_function_parameters(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
            )
        );
    }
    
    public static function multi_reprogram_adhoc_task_returns() {
        return new external_single_structure(
            array(
                'tasks' => new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT),
                        'status' => new external_value(PARAM_RAW)
                        )
                    )
                )
            )
        );
    }

    public static function multi_reprogram_adhoc_task($ids) {
        global $DB, $USER;

        if ($USER->id != 2) {
            throw new Exception('Only user admin can edit the data.');
        }

        list($insql, $inparams) = $DB->get_in_or_equal($ids);
        $DB->set_field_select(TaskManager::TABLE_ADHOC, 'status', TaskManager::STATUS_WAITING, 'id '.$insql, $inparams);
        $DB->set_field_select(TaskManager::TABLE_ADHOC, 'attempts', 0, 'id '.$insql, $inparams);
        $tasks = array();
        foreach($ids as $id) {
            $statusList = array_flip(TaskManager::get_status_list());

            $tasks[] = array(
                'id' => $id,
                'status' => '<span>'.$statusList[TaskManager::STATUS_WAITING].'</span>',
            );
        }

        return array(
            'tasks' => $tasks,
        );
    }

    public static function multi_delete_adhoc_task_parameters() {
        return new external_function_parameters(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
            )
        );
    }
    
    public static function multi_delete_adhoc_task_returns() {
        return new external_single_structure(
            array(
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_INT)
                ),
            )
        );
    }

    public static function multi_delete_adhoc_task($ids) {
        global $DB, $USER;

        if ($USER->id != 2) {
            throw new Exception('Only user admin can edit the data.');
        }

        list($insql, $inparams) = $DB->get_in_or_equal($ids);
        $DB->delete_records_select(TaskManager::TABLE_ADHOC, 'id '.$insql, $inparams);

        return array(
            'ids' => $ids,
        );
    }
    
    public static function get_adhoclogstasks_names() {
        return array_keys( $GLOBALS['DB']->get_records_sql('SELECT classname FROM {'.TaskManager::TABLE_ADHOC_LOG.'} GROUP BY classname') );
    }
    
    public static function get_adhoclogstasks_instances() {
        return array_keys( $GLOBALS['DB']->get_records_sql('SELECT instance FROM {'.TaskManager::TABLE_ADHOC_LOG.'} GROUP BY instance') );
    }

    public static function get_adhoctasks_names() {
        return array_keys( $GLOBALS['DB']->get_records_sql('SELECT classname FROM {'.TaskManager::TABLE_ADHOC.'} GROUP BY classname') );
    }

    public static function get_adhoctasks_instances() {
        return array_keys( $GLOBALS['DB']->get_records_sql('SELECT instance FROM {'.TaskManager::TABLE_ADHOC.'} GROUP BY instance') );
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static function get_stats_parameters() {
        return new external_function_parameters(
            array(
                'stattype'   => new external_value(PARAM_ALPHAEXT, ''),
                'begin'  => new external_value(PARAM_INT, ''),
                'end' => new external_value(PARAM_INT, ''),
                'steps'=> new external_value(PARAM_INT, ''),
            )
        );
    }
    
    public static function get_stats_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_BOOL),
                'json' => new external_value(PARAM_RAW, '', VALUE_OPTIONAL)
            )
        );
    }
    
    public static function get_stats($stattype, $begin, $end, $steps)
    {
        $stats = TaskMonitor::get_stats($stattype, $begin, $end, $steps);
        
        if ($stats !== false){
            $jstat = array();
            foreach ($stats AS $stat) {
                $jstat[$stat->step] = $stat->count;
            }
            
            return array('error'=>false,'json'=>json_encode($jstat));
        }
        
        return array('error'=>true);
    }
    
    
    
    
    
    
    
    public static function get_events_parameters() {
        return new external_function_parameters(
            array(
                'si' => new external_value(PARAM_INT),
                'ps' => new external_value(PARAM_INT),
                'so' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_events_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW)
            )
        );
    }
    
    public static function get_events($si, $ps, $so)
    {
        global $DB;
        
        $order = '';
        if (!empty($so)){
            $order = ' ORDER BY '.$so;
        }else{
            $order = ' ORDER BY timecreated DESC';
        }
        
        $query ='SELECT * FROM {'.TaskManager::TABLE_EVENT.'}'.$order.' LIMIT '.$si.','.$ps;
        $records = $DB->get_records_sql($query);
        
        $recordCount = $DB->get_record_sql('SELECT COUNT(*) AS found_rows FROM {'.TaskManager::TABLE_EVENT.'}')->found_rows;
        foreach($records AS $record) {
            $record->date = date(self::DATE_TIME_FORMAT, $record->date);
            $record->timecreated = date(self::DATE_TIME_FORMAT, $record->timecreated);
            
            $record->level = get_string('event_level_'.$record->level,'local_taskmanager');
            
            $record->type = get_string('event_type_'.$record->type,'local_taskmanager');
            
            $record->tasktype = get_string('worker_type_'.$record->tasktype,'local_taskmanager');
        }
        
        $jTableResult = array();
        $jTableResult['Result'] = 'OK';
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = array_values($records);
        
        return array('data'=>json_encode($jTableResult));
    }
    
    
    
    
    
}

