<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_taskmanager', language 'fr'
 *
 * @package   local_taskmanager
 * @copyright 2021 TCS
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginadministration'] = 'Gestionnaire des tâches';
$string['pluginname'] = 'Gestionnaire des tâches';
$string['pagedesc'] = 'Gestionnaire des tâches planifiées';
$string['pagemonitor'] = 'Task Monitor';
$string['adhoc_pagedesc'] = 'Gestionnaire des tâches adhoc';
$string['adhoc_history_pagedesc'] = 'Historique des tâches adhoc';
$string['schedule_history_pagedesc'] = 'Historique des tâches planifiées';
$string['preview_header_instance'] = 'Instance';
$string['preview_header_classname'] = 'Classname';
$string['preview_header_component'] = 'Component';
$string['preview_header_lastruntime'] = 'Last runtime';
$string['preview_header_nextruntime'] = 'Next runtime';
$string['preview_header_runtime'] = 'Runtime';
$string['preview_header_minute'] = 'Minute';
$string['preview_header_hour'] = 'Hour';
$string['preview_header_day'] = 'Day';
$string['preview_header_month'] = 'Month';
$string['preview_header_dayofweek'] = 'Day&nbsp;of&nbsp;week';
$string['preview_header_action'] = 'Action';
$string['preview_header_userid'] = 'User id';
$string['preview_header_duration'] = 'Duration';
$string['preview_header_result'] = 'Result';
$string['preview_header_enabled'] = 'Enabled';
$string['preview_header_status'] = 'Status';
$string['preview_header_laststatus'] = 'Last status';
$string['preview_header_vm'] = 'Vm';
$string['general'] = 'Configuration globale';

$string['mag_academy_path'] = 'Chemin des académies';
$string['mag_academy_config'] = 'Chemin de la configuration des académies';
$string['mag_auto_maintenance_path'] = 'Chemin du fichier de maintenance';
$string['maintenance_enabled'] = 'Activation de la maintenance';
$string['multidb_host'] = 'Adresse base de données';
$string['multidb_user'] = 'Utilisateur base de données';
$string['multidb_password'] = 'Mot de passe base de données';
$string['multidb_database_prefix'] = 'Préfix des bases de données';

$string['adhoc_task_enabled'] = 'Tâches adhoc activées';
$string['schedule_tasks_enabled'] = 'Tâches programmées activées';

$string['maintenance'] = 'Maintenance';
$string['synchronise_scheduled_tasks'] = 'Synchronisation des tâches programmées';
$string['multidb_database_prefix'] = 'Préfix base de données';

$string['tasks'] = 'Tasks';
$string['instances'] = 'Instances';
$string['status'] = 'Status';
$string['filter'] = 'Filter';

$string['tasks'] = 'Tasks';
$string['instances'] = 'Instances';
$string['status'] = 'Status';
$string['laststatus'] = 'Last status';
$string['filter'] = 'Filter';
$string['disable'] = 'Disable';
$string['enable'] = 'Enable';
$string['reset'] = 'Reset';
$string['edit'] = 'Edit';
$string['selectedlines'] = 'Selected lines';
$string['validate'] = 'Validate';


$string['scheduled_success'] = 'Scheduled Success';
$string['scheduled_failed'] = 'Scheduled Failed';
$string['scheduled_any'] = 'Scheduled All';
$string['adhoc_success'] = 'Adhoc Success';
$string['adhoc_failed'] = 'Adhoc Failed';
$string['adhoc_any'] = 'Adhoc ALl';

$string['event_level_2'] = 'Alert';
$string['event_level_3'] = 'Warning';

$string['event_type_2'] = 'Failed';
$string['event_type_3'] = 'Consecutive failed';

$string['worker_type_1'] = 'Adhoc';
$string['worker_type_2'] = 'Scheduled';
$string['worker_type_3'] = 'All';
$string['worker_type_4'] = 'Adhoc Collector';


$string['header_level'] = 'Level';
$string['header_type'] = 'Event';
$string['header_tasktype'] = 'Task type';
$string['header_taskid'] = 'Taskid';
$string['header_instance'] = 'Instance';
$string['header_classname'] = 'Classname';
$string['header_date'] = 'Date';
$string['header_timecreated'] = 'Date of event';


$string['monitor_task'] = 'Monitoring task';
$string['header_settings_events'] = 'Events';

$string['settings_alert_delay'] = 'Minimum delay between two alert notification';
$string['settings_alert_delay_desc'] = 'Minimum delay between two alert notification';
$string['settings_alert_nextrun_threshold'] = 'Delay before a task is considered late';
$string['settings_alert_nextrun_threshold_desc'] = 'Delay before a task is considered late';
$string['settings_alert_failed_threshold'] = 'Number of fail in the last 20 run before alert';
$string['settings_alert_failed_threshold_desc'] = 'Number of fail in the last 20 run before alert';
$string['settings_alert_consecutivefailed_threshold'] = 'Consecutive failed task before alert';
$string['settings_alert_consecutivefailed_threshold_desc'] = 'Consecutive failed task before alert';

$string['settings_warn_delay'] = 'Minimum delay between two warn notification';
$string['settings_warn_delay_desc'] = 'Minimum delay between two warn notification';
$string['settings_warn_nextrun_delay'] = 'Delay before a task is considered late';
$string['settings_warn_nextrun_delay_desc'] = 'Delay before a task is considered late';
$string['settings_warn_failed_threshold'] = 'Number of fail in the last 20 run before warn';
$string['settings_warn_failed_threshold_desc'] = 'Number of fail in the last 20 run before warn';
$string['settings_warn_consecutivefailed_threshold'] = 'Consecutive failed task before warn';
$string['settings_warn_consecutivefailed_threshold_desc'] = 'Consecutive failed task before warn';

$string['messageprovider:warn'] = 'Evènement warn';
$string['messageprovider:alert'] = 'Evènement alert';
