<?php

define('CLI_SCRIPT', true);

require_once(dirname(dirname(__DIR__)).'/config.php');
require_once($CFG->libdir.'/clilib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

list($options) = cli_get_params(
    array(
        'logdir' => false
    )
);
echo 'CACAHUETE';
$taskmanager = new TaskManager();
$taskmanager->setlogdir($options['logdir']);
$taskmanager->run_launcher();
