<?php

require_once(dirname(dirname(dirname(__DIR__))).'/config.php');

require_once($CFG->dirroot.'/local/taskmanager/externallib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

// DEFINE('TEXT_FIELD_SIZE', 6);
// DEFINE('TEXT_FIELD_MAX_SIZE', 25);

$context = context_system::instance();

require_capability('mod/taskmanager:view_adhoc', $context);

$readOnlyMode = ($USER->id != 2); 


$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/local/taskmanager/manage/adhoc.php');
$PAGE->set_pagetype('site-index');
$PAGE->set_pagelayout('standard');

$PAGE->set_title("Adhoc Task Scheduler");
$PAGE->set_heading("Adhoc Task Scheduler");

$strs = new stdClass();
$strs->preview_header_instance = get_string('preview_header_instance','local_taskmanager');
$strs->preview_header_classname = get_string('preview_header_classname','local_taskmanager');
$strs->preview_header_status = get_string('preview_header_status','local_taskmanager');
$strs->preview_header_nextruntime = get_string('preview_header_nextruntime','local_taskmanager');
$strs->preview_header_userid = get_string('preview_header_userid','local_taskmanager');
$strs->preview_header_action = get_string('preview_header_action','local_taskmanager');

$PAGE->requires->js_call_amd('local_taskmanager/form_adhoc', 'init', array('str'=>$strs, 'readOnlyMode' => $readOnlyMode));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string("adhoc_pagedesc", 'local_taskmanager'));

$tasks = local_taskmanager_external::get_adhoctasks_names();
$instance = local_taskmanager_external::get_adhoctasks_instances();
$status = TaskManager::get_status_list();


// // filter part
echo '
    <div class="form-group filters-group manage-adhoc-form">
        <div class="select-container">
            <div id="tasks-select" class="select-with-label">
                <label>'.get_string('tasks','local_taskmanager').'</label>
                '.html_writer::select(array_combine($tasks, $tasks), 'select-tasks', '', false, array('id'=>'select-tasks','class'=>'multi-select','multiple'=>'multiple')).'
            </div>
            <div id="instances-select" class="select-with-label">
                <label>'.get_string('instances','local_taskmanager').'</label>
                '.html_writer::select(array_combine($instance, $instance), 'select-instances', '', false, array('id'=>'select-instances','class'=>'multi-select','multiple'=>'multiple')).'          
            </div>
            <div id="status-select" class="select-with-label">
                <label>'.get_string('laststatus','local_taskmanager').'</label>
                '.html_writer::select(array_flip($status), 'select-status', '', false, array('id'=>'select-status','class'=>'multi-select','multiple'=>'multiple')).'
            </div>
            <div class="filter-button">
                <button class="btn btn-primary btn-block btn-filter" id="adhoc-filter" name="filter" type="button"><i class="fa fa-filter" aria-hidden="true"></i> '.get_string('filter','local_taskmanager').'</button>
            </div>
        </div>    
    </div>
';

// // table part
echo '
<div id="adhoctable">
</div>
';

if (!$readOnlyMode) {
    // multiedit part
    echo '
    <div>
        '.get_string('selectedlines','local_taskmanager').': <span id="nb-selected-rows">0</span>'.'
        <div class="form-group multiedit-group">
            <div><button class="btn btn-primary btn-block" id="multiedit-reprogram" name="reprogram" type="button">'.get_string('reprogram','local_taskmanager').'</button></div>
            <div><button class="btn btn-primary btn-block" id="multiedit-delete" name="delete" type="button">'.get_string('delete','local_taskmanager').'</button></div>
        </div>
    </div>';
}

echo $OUTPUT->footer();
