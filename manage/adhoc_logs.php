<?php

require_once(dirname(dirname(dirname(__DIR__))).'/config.php');

require_once($CFG->dirroot.'/local/taskmanager/externallib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

$context = context_system::instance();

require_capability('mod/taskmanager:view_adhoclogs', $context);

$readOnlyMode = ($USER->id != 2); 

$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/local/taskmanager/manage/adhoc_logs.php');
$PAGE->set_pagetype('site-index');
$PAGE->set_pagelayout('standard');

$PAGE->set_title("Task Manager - Task adhoc Logs");
$PAGE->set_heading("Task Manager - Task adhoc Logs");

$strs = new stdClass();
$strs->preview_header_instance = get_string('preview_header_instance','local_taskmanager');
$strs->preview_header_classname = get_string('preview_header_classname','local_taskmanager');
$strs->preview_header_component = get_string('preview_header_component','local_taskmanager');
$strs->preview_header_nextruntime = get_string('preview_header_nextruntime','local_taskmanager');
$strs->preview_header_startdate = get_string('preview_header_startdate','local_taskmanager');
$strs->preview_header_enddate = get_string('preview_header_enddate','local_taskmanager');
$strs->preview_header_status = get_string('preview_header_status','local_taskmanager');
$strs->preview_header_nbqueries = get_string('preview_header_nbqueries','local_taskmanager');
$strs->preview_header_exectime = get_string('preview_header_exectime','local_taskmanager');
$strs->preview_header_vm = get_string('preview_header_vm','local_taskmanager');
$strs->preview_header_worker = get_string('preview_header_worker','local_taskmanager');
$strs->preview_header_logs = get_string('preview_header_logs','local_taskmanager');

$PAGE->requires->js_call_amd('local_taskmanager/manage_adhoc_logs', 'init', array('str'=>$strs, 'readOnlyMode' => $readOnlyMode));

echo $OUTPUT->header();
echo $OUTPUT->heading("Task Manager - Task adhoc Logs");

$tasks = local_taskmanager_external::get_adhoclogstasks_names();
$instance = local_taskmanager_external::get_adhoclogstasks_instances();
$status = TaskManager::get_status_list();


// filter part
echo '<div class="form-group filters-group manage-adhoc-logs-form">';
echo '<div class="select-container">';
echo '<div id="tasks-select" class="select-with-label">';
echo '<label>'.get_string('tasks','local_taskmanager').'</label>';
echo html_writer::select(array_combine($tasks, $tasks), 'select-tasks', '', false, array('id'=>'select-tasks','class'=>'multi-select','multiple'=>'multiple'));
echo '</div>';
echo '<div id="instances-select" class="select-with-label">';
echo '<label>'.get_string('instances','local_taskmanager').'</label>';
echo html_writer::select(array_combine($instance, $instance), 'select-instances', '', false, array('id'=>'select-instances','class'=>'multi-select','multiple'=>'multiple'));
echo '</div>';
echo '<div id="status-select" class="select-with-label">';
echo '<label>'.get_string('status','local_taskmanager').'</label>';
echo html_writer::select(array_flip($status), 'select-status', '', false, array('id'=>'select-status','class'=>'multi-select','multiple'=>'multiple'));
echo '</div>';
echo '</div>';
echo '<div>
    <button class="btn btn-primary btn-block btn-filter" id="adhoc-logs-filter" name="filter" type="button"><i class="fa fa-filter" aria-hidden="true"></i> '.get_string('filter','local_taskmanager').'</button>
</div>
';
echo '</div>';

// table part
echo '<div id="adhoclogstable"></div>';

echo $OUTPUT->footer();
