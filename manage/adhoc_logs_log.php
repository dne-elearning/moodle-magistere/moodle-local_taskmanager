<?php

require_once(dirname(dirname(dirname(__DIR__))).'/config.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

require_login();

if ($USER->id != 2) {
    print_error('Acces denied');
}

$id = required_param('id', PARAM_INT);

$record = $DB->get_record(TaskManager::TABLE_ADHOC_LOG, array('id'=>$id), 'id, classname, status, startdate, enddate');

$recordlog = $DB->get_record(TaskManager::TABLE_ADHOC_LOGS, array('id'=>$id));

if ($record === false){
    die('ERROR: Task not found');
}

echo '<h1>Adhoc Task log "'.$record->classname.'"<h1><h2>'.strtoupper(get_string('status_'.$record->status, 'local_taskmanager')).' ('.date('d-m-Y H:i:s',$record->startdate).' => '.date('d-m-Y H:i:s',$record->enddate).')</h2>';
echo '<pre style="background-color:#CCCCCC">';
echo (isset($recordlog->logs)?$recordlog->logs:'NO LOG');
echo '</pre>';
