<?php

require_once(dirname(dirname(dirname(__DIR__))).'/config.php');

require_once($CFG->dirroot.'/local/taskmanager/externallib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskMonitor.php');

$context = context_system::instance();

require_capability('mod/taskmanager:view_event', $context);

$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/local/taskmanager/manage/event.php');
$PAGE->set_pagetype('site-index');
$PAGE->set_pagelayout('standard');

$PAGE->set_title('Task Event');
$PAGE->set_heading('Task Event');

$strs = new stdClass();
$strs->header_level = get_string('header_level','local_taskmanager');
$strs->header_type = get_string('header_type','local_taskmanager');
$strs->header_tasktype = get_string('header_tasktype','local_taskmanager');
$strs->header_taskid = get_string('header_taskid','local_taskmanager');
$strs->header_instance = get_string('header_instance','local_taskmanager');
$strs->header_classname = get_string('header_classname','local_taskmanager');
$strs->header_date = get_string('header_date','local_taskmanager');
$strs->header_timecreated = get_string('header_timecreated','local_taskmanager');


$PAGE->requires->js_call_amd('local_taskmanager/event', 'init', array('str'=>$strs));


echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pageevent', 'local_taskmanager'));

echo '<br/><br/><h3>Etat global</h3>';

echo '<b>'.TaskMonitor::get_waiting_scheduled().'</b> tâches scheduled en attente d\'exécution<br>';
echo '<b>'.TaskMonitor::get_todo_adhoc().'</b> tâches adhoc en attente d\'exécution<br>';
echo '<b>'.TaskMonitor::get_failed_adhoc().'</b> tâches adhoc en échec<br>';
echo '<b>'.TaskMonitor::get_waiting_adhoc().'</b> tâches adhoc en attente de leur date d\'exécution<br>';


echo '<br/><br/><h3>Historiques des évènements</h3>';
echo '<div id="eventtable"></div>';

echo $OUTPUT->footer();
