<?php

require_once(dirname(dirname(dirname(__DIR__))).'/config.php');

require_once($CFG->dirroot.'/local/taskmanager/externallib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

define('TEXT_FIELD_SIZE', 6);
define('TEXT_FIELD_MAX_SIZE', 25);

$context = context_system::instance();

require_capability('mod/taskmanager:view_scheduledlogs', $context);

$readOnlyMode = ($USER->id != 2); 


$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/local/taskmanager/manage/scheduled.php');
$PAGE->set_pagetype('site-index');
$PAGE->set_pagelayout('standard');

$PAGE->set_title('Task Manager - Scheduled');
$PAGE->set_heading('Task Manager - Scheduled');

$strs = new stdClass();
$strs->preview_header_instance = get_string('preview_header_instance','local_taskmanager');
$strs->preview_header_classname = get_string('preview_header_classname','local_taskmanager');
$strs->preview_header_component = get_string('preview_header_component','local_taskmanager');
$strs->preview_header_lastruntime = get_string('preview_header_lastruntime','local_taskmanager');
$strs->preview_header_nextruntime = get_string('preview_header_nextruntime','local_taskmanager');
$strs->preview_header_minute = get_string('preview_header_minute','local_taskmanager');
$strs->preview_header_hour = get_string('preview_header_hour','local_taskmanager');
$strs->preview_header_day = get_string('preview_header_day','local_taskmanager');
$strs->preview_header_month = get_string('preview_header_month','local_taskmanager');
$strs->preview_header_dayofweek = get_string('preview_header_dayofweek','local_taskmanager');
$strs->preview_header_action = get_string('preview_header_action','local_taskmanager');
$strs->preview_header_enabled = get_string('preview_header_enabled','local_taskmanager');
$strs->preview_header_status = get_string('preview_header_status','local_taskmanager');
$strs->preview_header_laststatus = get_string('preview_header_laststatus','local_taskmanager');
$strs->preview_header_vm = get_string('preview_header_vm','local_taskmanager');


$PAGE->requires->js_call_amd('local_taskmanager/form_scheduled', 'init', array('str'=>$strs, 'readOnlyMode' => $readOnlyMode));

//admin_externalpage_setup('mod_centraladmin_slave_manager');


echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pagedesc', 'local_taskmanager'));

$tasks = local_taskmanager_external::get_scheduledtasks_names();
$instance = local_taskmanager_external::get_scheduledtasks_instances();
$laststatus = TaskManager::get_laststatus_list();


// filter part
echo '
<div class="form-group filters-group manage-scheduled-form">
    <div class="select-container">
        <div id="tasks-select" class="select-with-label">
            <label>'.get_string('tasks','local_taskmanager').'</label>
            '.html_writer::select(array_combine($tasks, $tasks), 'select-tasks', '', false, array('id'=>'select-tasks','class'=>'multi-select','multiple'=>'multiple')).'
        </div>
        <div id="instances-select" class="select-with-label">
            <label>'.get_string('instances','local_taskmanager').'</label>
            '.html_writer::select(array_combine($instance, $instance), 'select-instances', '', false, array('id'=>'select-instances','class'=>'multi-select','multiple'=>'multiple')).'
        </div>
        <div id="status-select" class="select-with-label">
            <label>'.get_string('laststatus','local_taskmanager').'</label>
            '.html_writer::select(array_flip($laststatus), 'select-status', '', false, array('id'=>'select-status','class'=>'multi-select','multiple'=>'multiple')).'
        </div>
        <div class="filter-button">
            <button class="btn btn-primary btn-block btn-filter" id="scheduled-filter" name="filter" type="button"><i class="fa fa-filter" aria-hidden="true"></i> '.get_string('filter','local_taskmanager').'</button>
        </div>
    </div>
</div>';

// table part
echo '<div id="scheduletable"></div>';

if (!$readOnlyMode) {
    // multiedit part
    echo '
    <div>
        '.get_string('selectedlines','local_taskmanager').': <span id="nb-selected-rows">0</span>'.'
    </div>
    <div class="form-group multiedit-group">
        <div><button class="btn btn-primary btn-block" id="multiedit-reset" name="reset" type="button">'.get_string('reset','local_taskmanager').'</button></div>
        <div><button class="btn btn-primary btn-block" id="multiedit-disable" name="disable" type="button">'.get_string('disable','local_taskmanager').'</button></div>
        <div><button class="btn btn-primary btn-block" id="multiedit-enable" name="enable" type="button">'.get_string('enable','local_taskmanager').'</button></div>
        <div><button class="btn btn-primary btn-block" id="multiedit-edit" name="edit" type="button">'.get_string('edit','local_taskmanager').'</button></div>
    </div>

    <div class="form-group multiedit-group" id="multiedit-editform">
        <div class="multiedit-timeelement">
            <label>'.get_string('preview_header_minute','local_taskmanager').'</label>
            <input size="'.TEXT_FIELD_SIZE.'" maxlength="'.TEXT_FIELD_MAX_SIZE.'" id="multi-edit-minute" name="multi-edit-minute" value="" />
        </div>
        <div class="multiedit-timeelement">
            <label>'.get_string('preview_header_hour','local_taskmanager').'</label>
            <input size="'.TEXT_FIELD_SIZE.'" maxlength="'.TEXT_FIELD_MAX_SIZE.'" id="multi-edit-hour" name="multi-edit-hour" value="" />
        </div>
        <div class="multiedit-timeelement">
            <label>'.get_string('preview_header_day','local_taskmanager').'</label>
            <input size="'.TEXT_FIELD_SIZE.'" maxlength="'.TEXT_FIELD_MAX_SIZE.'" id="multi-edit-day" name="multi-edit-day" value="" />
        </div>
        <div class="multiedit-timeelement">
            <label>'.get_string('preview_header_month','local_taskmanager').'</label>
            <input size="'.TEXT_FIELD_SIZE.'" maxlength="'.TEXT_FIELD_MAX_SIZE.'" id="multi-edit-month" name="multi-edit-month" value="" />
        </div>
        <div class="multiedit-timeelement">
            <label>'.get_string('preview_header_dayofweek','local_taskmanager').'</label>
            <input size="'.TEXT_FIELD_SIZE.'" maxlength="'.TEXT_FIELD_MAX_SIZE.'" id="multi-edit-dayofweek" name="multi-edit-dayofweek" value="" />
        </div>
        <div><button class="btn btn-primary btn-block" id="multiedit-validate-edit" name="multiedit-validate-edit" type="button">'.get_string('validate','local_taskmanager').'</button></div>
    </div>';
}



echo $OUTPUT->footer();
