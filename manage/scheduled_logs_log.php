<?php

require_once(dirname(dirname(dirname(__DIR__))).'/config.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

require_login();

if ($USER->id != 2) {
    print_error('Acces denied');
}

$id = required_param('id', PARAM_INT);

//$record = $DB->get_record(TaskManager::TABLE_SCHEDULED_LOG, array('id'=>$id), 'id, status, startdate, enddate, logs');

$record = $DB->get_record_sql("SELECT sl.id, s.classname, sl.status, sl.startdate, sl.enddate
FROM {".TaskManager::TABLE_SCHEDULED_LOG."} sl
INNER JOIN {".TaskManager::TABLE_SCHEDULED."} s ON (s.id=sl.scheduled_id) 
WHERE sl.id = :id", array('id'=>$id));

$recordlog = $DB->get_record(TaskManager::TABLE_SCHEDULED_LOGS, array('id'=>$id));

if ($record === false){
    die('ERROR: Task not found');
}

echo '<h1>Scheduled Task log "'.$record->classname.'"<h1><h2>'.strtoupper(get_string('status_'.$record->status, 'local_taskmanager')).' ('.date('d-m-Y H:i:s',$record->startdate).' => '.date('d-m-Y H:i:s',$record->enddate).')</h2>';
echo '<pre style="background-color:#CCCCCC">';
echo (isset($recordlog->logs)?$recordlog->logs:'NO LOG');
echo '</pre>';
