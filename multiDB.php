<?php

// multiDB::instance()->get('ac-paris')->get_record();
// require_once($CFG->dirroot.'/local/taskmanager/multiDB.php');

class multiDB
{
	private static $static_singleton = null;
	private static $plugin_name = 'local_taskmanager';
	
	// singletons
	private $dbConnection = null;
	private $currentDB = null;
	
	/**
	 * Return the singleton
	 */
	static function instance()
	{
		
		
		$multidb_host = get_config(self::$plugin_name, 'multidb_host');
		$multidb_user = get_config(self::$plugin_name, 'multidb_user');
		$multidb_password = get_config(self::$plugin_name, 'multidb_password');
		if (!( $multidb_host && strlen($multidb_host) > 3
			&& $multidb_user && strlen($multidb_user) > 3
			&& $multidb_password && strlen($multidb_password) > 3)
		) {
			self::l('Error: the multi DB configuration is not set properly.');
			die;
		}
		

		if (multiDB::$static_singleton == null)
		{
			multiDB::$static_singleton = new multiDB();
		}
		return multiDB::$static_singleton;
	}
	
	protected function __construct()
	{
	}
	
	/***
	 * 
	 * @param unknown $academie_name The academy name (ex: ac-paris, ac-nice)
	 * @return mixed Return the moodle_database instance or false is the connection failed
	 */
	function get($academy_name)
	{
		global $CFG;
		
		$academies = \TaskManager::get_magistere_acas();
		if (!array_key_exists($academy_name,$academies))
		{
			return false;
		}
		
		$academy = $academies[$academy_name];
		$database_name = get_config(self::$plugin_name,'multidb_database_prefix').$academy['shortname'];

		if ($this->currentDB == $database_name) {
			return $this->dbConnection;
		}else if ($this->dbConnection != null) {
			$this->dbConnection->execute("USE ".$database_name);
		}else{
			$this->dbConnection = $this->dbConnect(get_config(self::$plugin_name, 'multidb_host'), get_config(self::$plugin_name, 'multidb_user'), get_config(self::$plugin_name, 'multidb_password'), $database_name);
		}

		$this->currentDB = $database_name;
		
		return $this->dbConnection;
	}

	private function dbConnect($host,$user,$password,$database)
	{
		global $CFG;
		
		$connection = null;
		
		if (!$connection = moodle_database::get_driver_instance($CFG->dbtype, $CFG->dblibrary)) {
			throw new dml_exception('dbdriverproblem', "Unknown driver $CFG->dblibrary/$CFG->dbtype");
		}

		try {
			$connection->connect($host, $user, $password, $database, $CFG->prefix, $CFG->dboptions);
		} catch (moodle_exception $e) {
			error_log($database.'##'.__FILE__.'##'.$e->getMessage());
			throw $e;
		}
	
		return $connection;
	}

	private static function l($msg, $return = true){
        echo date('Ymd_His__').$msg.($return?"\n":"");
    }
	
}
