<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   local_taskmanager
 * @copyright TCS 2021
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;


if ($hassiteconfig) {
    $settings = new admin_settingpage(
        'local_taskmanager',
        get_string('pluginname', 'local_taskmanager')
    );

    // general settings
    $settings->add(
        new admin_setting_heading(
            'local_taskmanager/general',
            get_string('general', 'local_taskmanager'),
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/mag_academy_path',
            get_string('mag_academy_path', 'local_taskmanager'),
            '',
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/mag_academy_config',
            get_string('mag_academy_config', 'local_taskmanager'),
            '',
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/mag_auto_maintenance_path',
            get_string('mag_auto_maintenance_path', 'local_taskmanager'),
            '',
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/multidb_host',
            get_string('multidb_host', 'local_taskmanager'),
            '',
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/multidb_user',
            get_string('multidb_user', 'local_taskmanager'),
            '',
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/multidb_password',
            get_string('multidb_password', 'local_taskmanager'),
            '',
            ''
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/multidb_database_prefix',
            get_string('multidb_database_prefix', 'local_taskmanager'),
            '',
            'moodle_'
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_taskmanager/adhoc_task_enabled',
            get_string('adhoc_task_enabled', 'local_taskmanager'),
            '',
            '1'
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_taskmanager/schedule_tasks_enabled',
            get_string('schedule_tasks_enabled', 'local_taskmanager'),
            '',
            '1'
        )
    );


    // general settings
    $settings->add(
        new admin_setting_heading(
            'local_taskmanager/maintenance',
            get_string('maintenance', 'local_taskmanager'),
            ''
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_taskmanager/maintenance_enabled',
            get_string('maintenance_enabled', 'local_taskmanager'),
            '',
            '0'
        )
    );
    
    
    $settings->add(
        new admin_setting_heading(
            'local_taskmanager/events',
            get_string('header_settings_events', 'local_taskmanager'),
            ''
        )
    );
    
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/alert_delay',
            get_string('settings_alert_delay', 'local_taskmanager'),
            get_string('settings_alert_delay_desc', 'local_taskmanager'),
            '21600',
            PARAM_INT
        )
    );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/alert_nextrun_threshold',
            get_string('settings_alert_nextrun_threshold', 'local_taskmanager'),
            get_string('settings_alert_nextrun_threshold_desc', 'local_taskmanager'),
            '21600',
            PARAM_INT
        )
    );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/alert_failed_threshold',
            get_string('settings_alert_failed_threshold', 'local_taskmanager'),
            get_string('settings_alert_failed_threshold_desc', 'local_taskmanager'),
            '10',
            PARAM_INT
        )
    );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/alert_consecutivefailed_threshold',
            get_string('settings_alert_consecutivefailed_threshold', 'local_taskmanager'),
            get_string('settings_alert_consecutivefailed_threshold_desc', 'local_taskmanager'),
            '5',
            PARAM_INT
        )
        );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/warn_delay',
            get_string('settings_warn_delay', 'local_taskmanager'),
            get_string('settings_warn_delay_desc', 'local_taskmanager'),
            '3600',
            PARAM_INT
            )
        );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/warn_nextrun_delay',
            get_string('settings_warn_nextrun_delay', 'local_taskmanager'),
            get_string('settings_warn_nextrun_delay_desc', 'local_taskmanager'),
            '10800',
            PARAM_INT
        )
    );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/warn_failed_threshold',
            get_string('settings_warn_failed_threshold', 'local_taskmanager'),
            get_string('settings_warn_failed_threshold_desc', 'local_taskmanager'),
            '5',
            PARAM_INT
        )
    );
    
    $settings->add(
        new admin_setting_configtext(
            'local_taskmanager/warn_consecutivefailed_threshold',
            get_string('settings_warn_consecutivefailed_threshold', 'local_taskmanager'),
            get_string('settings_warn_consecutivefailed_threshold_desc', 'local_taskmanager'),
            '3',
            PARAM_INT
        )
    );

    $ADMIN->add('localplugins', $settings);
}

