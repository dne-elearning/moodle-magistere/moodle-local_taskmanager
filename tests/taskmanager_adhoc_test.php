<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Basic unit tests for local_taskmanager.
 *
 * @package     local_taskmanager
 * @copyright   2022 TCS
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */




defined('MOODLE_INTERNAL') || die();


class taskmanager_adhoc_testcase extends advanced_testcase {

    protected function setUp(){
        global $CFG, $DB;
        require_once($CFG->dirroot.'/local/taskmanager/externallib.php');
        require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');
        
        $this->resetAfterTest();

        // adhoc tasks
        $task1 = new stdClass();
        $task1->instance = 'ac-versailles';
        $task1->taskid = 1;
        $task1->classname = 'test1';
        $task1->nextRuntime = 0;
        $task1->customdata = '';
        $task1->vmid = null;
        $task1->status = TaskManager::STATUS_FAILED;
        $task1->workerid = null;
        $task1->attempts = 5;

        $task2 = new stdClass();
        $task2->instance = 'ac-versailles';
        $task2->taskid = 2;
        $task2->classname = 'test2';
        $task2->nextRuntime = 0;
        $task2->customdata = '';
        $task2->vmid = null;
        $task2->status = TaskManager::STATUS_WAITING;
        $task2->workerid = null;
        $task2->attempts = 4;

        $task3 = new stdClass();
        $task3->instance = 'ac-caen';
        $task3->taskid = 3;
        $task3->classname = 'test3';
        $task3->nextRuntime = 0;
        $task3->customdata = '';
        $task3->vmid = null;
        $task3->status = TaskManager::STATUS_WAITING;
        $task3->workerid = null;
        $task3->attempts = 5;

        $task4 = new stdClass();
        $task4->instance = 'ac-caen';
        $task4->taskid = 4;
        $task4->classname = 'test4';
        $task4->nextRuntime = 0;
        $task4->customdata = '';
        $task4->vmid = null;
        $task4->status = TaskManager::STATUS_WAITING;
        $task4->workerid = null;
        $task4->attempts = 0;

        $adhoc_tasks = array(
            $task1,
            $task2,
            $task3,
            $task4,
        );
        $DB->insert_records(TaskManager::TABLE_ADHOC, $adhoc_tasks);
    }

    public function test_get_adhoctasks() {
        // test that we get all the tasks
        $res = local_taskmanager_external::get_adhoctasks(null, null, null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 4);

        // same with empty array
        $res = local_taskmanager_external::get_adhoctasks(array(), array(), array(), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 4);

        // filter by instance
        $res = local_taskmanager_external::get_adhoctasks(null, array('ac-versailles'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 2);
        $this->assertEquals($tasks->Records[0]->classname, 'test1');
        $this->assertEquals($tasks->Records[1]->classname, 'test2');

        // with multiple instance
        $res = local_taskmanager_external::get_adhoctasks(null, array('ac-versailles', 'ac-caen'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 4);

        // by tasks
        $res = local_taskmanager_external::get_adhoctasks(array('test4'), null, null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 1);
        $this->assertEquals($tasks->Records[0]->classname, 'test4');

        // double filter
        $res = local_taskmanager_external::get_adhoctasks(array('test4'), array('ac-versailles', 'ac-caen'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 1);
        $this->assertEquals($tasks->Records[0]->classname, 'test4');

        // with no results
        $res = local_taskmanager_external::get_adhoctasks(array('test4'), array('ac-versailles'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 0);

        // status
        $res = local_taskmanager_external::get_adhoctasks(null, null, array(TaskManager::STATUS_WAITING), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 3);
        $this->assertEquals($tasks->Records[0]->classname, 'test2');
        $this->assertEquals($tasks->Records[1]->classname, 'test3');
        $this->assertEquals($tasks->Records[2]->classname, 'test4');

        // all filter
        $res = local_taskmanager_external::get_adhoctasks(array('test4'), array('ac-versailles', 'ac-caen'), array(TaskManager::STATUS_WAITING), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 1);
        $this->assertEquals($tasks->Records[0]->classname, 'test4');

        // with no results
        $res = local_taskmanager_external::get_adhoctasks(array('test4'), array('ac-versailles'), array(TaskManager::STATUS_SUCCESS), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 0);
    }

    public function test_multi_reprogram_adhoc_task() {
        global $DB;

        $this->setUser(2); // we need to be admin 
        $task1 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 1));

        $res = local_taskmanager_external::multi_reprogram_adhoc_task(array($task1->id));
        $this->assertEquals($res['tasks'][0]['id'], $task1->id);
        $this->assertEquals($res['tasks'][0]['status'], '<span>WAITING</span>');

        // check value in db
        $task1 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 1));
        $this->assertEquals($task1->status, TaskManager::STATUS_WAITING);
        $this->assertEquals($task1->attempts, 0);


        $task2 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 2));
        $task3 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 3));

        $res = local_taskmanager_external::multi_reprogram_adhoc_task(array($task2->id, $task3->id));
        $this->assertEquals($res['tasks'][0]['id'], $task2->id);
        $this->assertEquals($res['tasks'][0]['status'], '<span>WAITING</span>');
        $this->assertEquals($res['tasks'][1]['id'], $task3->id);
        $this->assertEquals($res['tasks'][1]['status'], '<span>WAITING</span>');

        $task2 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 2));
        $task3 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 3));

        $this->assertEquals($task2->status, TaskManager::STATUS_WAITING);
        $this->assertEquals($task2->attempts, 0);

        $this->assertEquals($task3->status, TaskManager::STATUS_WAITING);
        $this->assertEquals($task3->attempts, 0);
    }

    public function test_multi_delete_adhoc_task() {
        global $DB, $USER;

        $this->setUser(2); // we need to be admin 

        $task1 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 1));

        // delete one task
        $res = local_taskmanager_external::multi_delete_adhoc_task(array($task1->id));
        
        $this->assertEquals($res['ids'][0], $task1->id);
        $task1 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 1));
        $this->assertFalse($task1);

        // others must still exist
        $task2 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 2));
        $task3 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 3));
        $this->assertNotFalse($task2);
        $this->assertNotFalse($task3);

        // delete many tasks
        $res = local_taskmanager_external::multi_delete_adhoc_task(array($task2->id, $task3->id));
        $this->assertEquals($res['ids'][0], $task2->id);
        $this->assertEquals($res['ids'][1], $task3->id);
        $task2 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 2));
        $task3 = $DB->get_record(TaskManager::TABLE_ADHOC, array('taskid' => 3));
        $this->assertFalse($task2);
        $this->assertFalse($task3);
    }

}