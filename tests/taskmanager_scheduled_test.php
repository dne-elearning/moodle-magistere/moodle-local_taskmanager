<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Scheduled task unit tests for local_taskmanager.
 *
 * @package     local_taskmanager
 * @copyright   2022 TCS
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


class taskmanager_scheduled_testcase extends advanced_testcase {

    protected function setUp(){
        global $CFG, $DB;
        require_once($CFG->dirroot.'/local/taskmanager/externallib.php');
        require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');
        
        $this->resetAfterTest();

        // scheduled tasks
        $task1 = new stdClass();
        $task1->instance = 'ac-versailles';
        $task1->component = 'test';
        $task1->classname = 'test1';
        $task1->disabled = 0;
        $task1->lastruntime = 0;
        $task1->nextRuntime = 0;
        $task1->minute = '*';
        $task1->hour = '*';
        $task1->day = '*';
        $task1->month = '*';
        $task1->dayofweek = '*';
        $task1->vmid = null;
        $task1->status = TaskManager::STATUS_WAITING;
        $task1->workerid = null;
        $task1->laststatus = TaskManager::STATUS_FAILED;

        $task2 = new stdClass();
        $task2->instance = 'ac-versailles';
        $task2->component = 'test';
        $task2->classname = 'test2';
        $task2->disabled = 0;
        $task2->lastruntime = 0;
        $task2->nextRuntime = 0;
        $task2->minute = '*';
        $task2->hour = '*';
        $task2->day = '*';
        $task2->month = '*';
        $task2->dayofweek = '*';
        $task2->vmid = null;
        $task2->status = TaskManager::STATUS_WAITING;
        $task2->workerid = null;
        $task2->laststatus = TaskManager::STATUS_FAILED;

        $task3 = new stdClass();
        $task3->instance = 'ac-caen';
        $task3->component = 'test';
        $task3->classname = 'test1';
        $task3->disabled = 0;
        $task3->lastruntime = 0;
        $task3->nextRuntime = 0;
        $task3->minute = '*';
        $task3->hour = '*';
        $task3->day = '*';
        $task3->month = '*';
        $task3->dayofweek = '*';
        $task3->vmid = null;
        $task3->status = TaskManager::STATUS_WAITING;
        $task3->workerid = null;
        $task3->laststatus = TaskManager::STATUS_SUCCESS;

        $task4 = new stdClass();
        $task4->instance = 'ac-caen';
        $task4->component = 'test';
        $task4->classname = 'test4';
        $task4->disabled = 0;
        $task4->lastruntime = 0;
        $task4->nextRuntime = 0;
        $task4->minute = '*';
        $task4->hour = '*';
        $task4->day = '*';
        $task4->month = '*';
        $task4->dayofweek = '*';
        $task4->vmid = null;
        $task4->status = TaskManager::STATUS_WAITING;
        $task4->workerid = null;
        $task4->laststatus = TaskManager::STATUS_SUCCESS;

        $task5 = new stdClass();
        $task5->instance = 'ac-dijon';
        $task5->component = 'test';
        $task5->classname = 'test5';
        $task5->disabled = 0;
        $task5->lastruntime = 0;
        $task5->nextRuntime = 0;
        $task5->minute = '*';
        $task5->hour = '*';
        $task5->day = '*';
        $task5->month = '*';
        $task5->dayofweek = '*';
        $task5->vmid = null;
        $task5->status = TaskManager::STATUS_WAITING;
        $task5->workerid = null;
        $task5->laststatus = TaskManager::STATUS_SUCCESS;

        $scheduled_tasks = array(
            $task1,
            $task2,
            $task3,
            $task4,
            $task5,
        );
        $DB->insert_records(TaskManager::TABLE_SCHEDULED, $scheduled_tasks);    
    }

    /**
     *  Test de la fonction get_scheduledtasks()
     */
    public function test_get_scheduledtasks() {
        // test that we get all the tasks
        $res = local_taskmanager_external::get_scheduledtasks(null, null, null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 5);

        // same with empty array
        $res = local_taskmanager_external::get_scheduledtasks(array(), array(), array(), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 5);

        // filter by instance
        $res = local_taskmanager_external::get_scheduledtasks(null, array('ac-versailles'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 2);
        $this->assertEquals($tasks->Records[0]->classname, 'test1');
        $this->assertEquals($tasks->Records[1]->classname, 'test2');

        // with multiple instance
        $res = local_taskmanager_external::get_scheduledtasks(null, array('ac-versailles', 'ac-caen'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 4);

        // by tasks
        $res = local_taskmanager_external::get_scheduledtasks(array('test4'), null, null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 1);
        $this->assertEquals($tasks->Records[0]->classname, 'test4');

        // double filter
        $res = local_taskmanager_external::get_scheduledtasks(array('test4'), array('ac-versailles', 'ac-caen'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 1);
        $this->assertEquals($tasks->Records[0]->classname, 'test4');

        // with no results
        $res = local_taskmanager_external::get_scheduledtasks(array('test4'), array('ac-versailles'), null, 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 0);

        // status
        $res = local_taskmanager_external::get_scheduledtasks(null, null, array(TaskManager::STATUS_SUCCESS), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 3);
        $this->assertEquals($tasks->Records[0]->classname, 'test1');
        $this->assertEquals($tasks->Records[1]->classname, 'test4');

        // all filter
        $res = local_taskmanager_external::get_scheduledtasks(array('test4'), array('ac-versailles', 'ac-caen'), array(TaskManager::STATUS_SUCCESS), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 1);
        $this->assertEquals($tasks->Records[0]->classname, 'test4');

        // with no results
        $res = local_taskmanager_external::get_scheduledtasks(array('test4'), array('ac-versailles'), array(TaskManager::STATUS_SUCCESS), 0, 1000, null);
        $tasks = json_decode($res['data']);
        $this->assertEquals(count($tasks->Records), 0);
    }

    // multi_reset_st_nextruntime // not test yet because not much to test if we can't check time

    public function test_multi_disable_st() {
        global $DB;

        $this->setUser(2); // we need to be admin 
        $task1 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test2'));

        $res = local_taskmanager_external::multi_disable_st(array($task1->id));
        $this->assertEquals($res['tasks'][0]['id'], $task1->id);
        $this->assertEquals($res['tasks'][0]['disabled'], 1);

        // check value in db
        $task1 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test2'));
        $this->assertEquals($task1->disabled, 1);

        // multi
        $task4 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test4'));
        $task5 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test5'));


        $res = local_taskmanager_external::multi_disable_st(array($task4->id, $task5->id));
        $this->assertEquals($res['tasks'][0]['id'], $task4->id);
        $this->assertEquals($res['tasks'][0]['disabled'], 1);
        $this->assertEquals($res['tasks'][1]['id'], $task5->id);
        $this->assertEquals($res['tasks'][1]['disabled'], 1);

        $task4 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test4'));
        $task5 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test5'));
        $this->assertEquals($task4->disabled, 1);
        $this->assertEquals($task5->disabled, 1);
    }

    public function test_multi_enable_st() {
        global $DB;

        $this->setUser(2); // we need to be admin 

        $task1 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test2'));

        $res = local_taskmanager_external::multi_enable_st(array($task1->id));
        $this->assertEquals($res['tasks'][0]['id'], $task1->id);
        $this->assertEquals($res['tasks'][0]['disabled'], 0);

        // check value in db
        $task1 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test2'));
        $this->assertEquals($task1->disabled, 0);

        // multi
        $task4 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test4'));
        $task5 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test5'));

        $res = local_taskmanager_external::multi_enable_st(array($task4->id, $task5->id));
        $this->assertEquals($res['tasks'][0]['id'], $task4->id);
        $this->assertEquals($res['tasks'][0]['disabled'], 0);
        $this->assertEquals($res['tasks'][1]['id'], $task5->id);
        $this->assertEquals($res['tasks'][1]['disabled'], 0);

        $task4 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test4'));
        $task5 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test5'));
        $this->assertEquals($task4->disabled, 0);
        $this->assertEquals($task5->disabled, 0);
    }

    // update_st_rows
    public function test_update_st_rows() {
        global $DB;

        $this->setUser(2); // we need to be admin 

        $task1 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test2'));
        
        // one update
        $res = local_taskmanager_external::update_st_rows(array($task1->id), '*/60', '*/24', '*/365', '*/12', '*/7', 1, 2);
        $this->assertEquals(count($res['tasks']), 1);
        $resTask = $res['tasks'][0];
        $this->assertEquals($resTask['id'], $task1->id);
        $this->assertEquals($resTask['minute'], '*/60');
        $this->assertEquals($resTask['hour'], '*/24');
        $this->assertEquals($resTask['day'], '*/365');
        $this->assertEquals($resTask['month'], '*/12');
        $this->assertEquals($resTask['dayofweek'], '*/7');
        $this->assertEquals($resTask['disabled'], 1);
        $this->assertEquals($resTask['vmid'], 2);

        // many updates
        $task4 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test4'));
        $task5 = $DB->get_record(TaskManager::TABLE_SCHEDULED, array('classname' => 'test5'));
        $res = local_taskmanager_external::update_st_rows(array($task4->id, $task5->id), '*/60', '*/24', '*/365', '*/12', '*/7', 1, 2);
        $this->assertEquals(count($res['tasks']), 2);
        $resTask1 = $res['tasks'][0];
        $this->assertEquals($resTask1['id'], $task4->id);
        $this->assertEquals($resTask1['minute'], '*/60');
        $this->assertEquals($resTask1['hour'], '*/24');
        $this->assertEquals($resTask1['day'], '*/365');
        $this->assertEquals($resTask1['month'], '*/12');
        $this->assertEquals($resTask1['dayofweek'], '*/7');
        $this->assertEquals($resTask1['disabled'], 1);
        $this->assertEquals($resTask1['vmid'], 2);

        $resTask2 = $res['tasks'][1];
        $this->assertEquals($resTask2['id'], $task5->id);
        $this->assertEquals($resTask2['minute'], '*/60');
        $this->assertEquals($resTask2['hour'], '*/24');
        $this->assertEquals($resTask2['day'], '*/365');
        $this->assertEquals($resTask2['month'], '*/12');
        $this->assertEquals($resTask2['dayofweek'], '*/7');
        $this->assertEquals($resTask2['disabled'], 1);
        $this->assertEquals($resTask2['vmid'], 2);
    }


}