<?php

define('CLI_SCRIPT', true);

require_once(dirname(dirname(__DIR__)).'/config.php');
require_once($CFG->libdir.'/clilib.php');
require_once($CFG->dirroot.'/local/taskmanager/TaskManager.php');

list($options) = cli_get_params(
    array(
        'workerid' => false
    )
);

$taskmanager = new TaskManager();
$taskmanager->setup_worker($options['workerid']);
$taskmanager->run_worker();
